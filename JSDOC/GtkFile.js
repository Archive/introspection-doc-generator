//<script type="text/javscript">

XObject = imports.XObject.XObject;


 
console = imports.console.console; 

// make sure parent is loaded..
RooFile = imports.RooFile..RooFile;

/**
 * 
 * this takes our collased file, and turns it into the config array..
 * 
 * 
 * 
 * 
 * 
 * 
 */



GtkFile  = XObject.define(
    function (ar)
    {
        GtkFile.superclass.constructor.call(this, ar);
       // console.log("STARTING OUTPUT");
        
        

    }, 
    RooFile, 
    {
        
        cfg : { },
        giImports : [],
        imports : { }, 
        parse: function()
        {
            // parse imports...
            
            this.parseImports();
            // function create() { return;
                
            var pos = this.lookFor( 'function');
            
            
            // standard create format.
            
            if (pos  > -1 && this.tokens[pos+1].data == 'create') {
                var tree = this.tokens[pos+3].items[0][1].props;
                this.cfg= this.parseProps(tree);
                return;
            }
            GtkFile.superclass.parse.call(this);
            
            
            
            
            
             
            
            
                
        },
        
        parseImports : function()
        {
           // console.dump(this.tokens);
            this.giImports = [];
            while (true) {
                var pos = this.lookFor('=');
                if (pos < 0)  {
                    break;
                }
                this.cursor = pos;
                var k = this.look(-1, true);
                var v = this.look(1, true);
               // Seed.print(k.data + " => " + v.data);
               
                /// finish!!! - not an import ...
               
                if (!v.data.match(/^imports/)) {
                    return; ///
                    
                    this.cursor++;
                    continue;
                }
                if (v.data.match(/^imports\.gi/)) {
                    // gi import..
                    this.giImports.push(v.data.replace(/imports\.gi\./, ''));
                    
                    this.cursor++;
                    continue;
                }
                  
                // two types of import left
                // imports.xnew
                if (v.data.match(/^imports\./)) {
                    this.imports[k.data] = v.data.replace(/imports\./, '') + '.js';
                    this.cursor++;
                    continue;
                }
                // imports[.....]
                this.cursor++;
                if (this.lookFor('[') > this.lookFor('=')) {
                    continue;
                }
                var bpos = this.lookFor('[');
               // console.dump(this.tokens[bpos]);
                
                this.imports[k.data] = this.tokens[bpos].items[0][0].toJS();
                
                this.cursor++;
                
            }
        //    console.dump(this.giImports);
          //  console.dump(this.imports);
            //Seed.quit();
            
        }
        
});
    