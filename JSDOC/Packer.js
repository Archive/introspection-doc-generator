// <script type="text/javascript">
XObject         = imports.XObject.XObject;
File            = imports.File.File;

TextStream      = imports.TextStream.TextStream;
TokenReader     = imports.TokenReader.TokenReader;
ScopeParser     = imports.ScopeParser.ScopeParser;
TokenStream     = imports.TokenStream.TokenStream;
CompressWhite   = imports.CompressWhite.CompressWhite;
Collapse        = imports.Collapse.Collapse;

GLib = imports.gi.GLib;
/**
 * @namespace JSDOC
 * @class  Packer
 * Create a new packer
 * 
 * Use with pack.js 
 * 
 * 
 * Usage:
 * <code>
 *
Packer = imports['JSDOC/Packer.js'].Packer;
var x = new  Packer({
    
    files : [ "/location/of/file1.js", "/location/of/file2.js", ... ],
    target : "/tmp/output.js",
    debugTarget : "/tmp/output.debug.js", // merged file without compression.
    translateJSON: "/tmp/translate.json",
    
    
);
x.packFiles(
    "/location/of/temp_batch_dir", 
    "/location/of/output-compacted-file.js",
    "/location/of/output-debug-merged-file.js"
);
    
 *</code> 
 *
 * Notes for improving compacting:
 *  if you add a jsdoc comment 
 * <code>
 * /**
 *   eval:var:avarname
 *   eval:var:bvarname
 *   ....
 * </code>
 * directly before an eval statement, it will compress all the code around the eval, 
 * and not rename the variables 'avarname'
 * 
 * Dont try running this on a merged uncompressed large file - it's used to be horrifically slow. not sure about now..
 * Best to use lot's of small classes, and use it to merge, as it will cache the compaction
 * 
 * 
 * 
 * Notes for translation
 *  - translation relies on you using double quotes for strings if they need translating
 *  - single quoted strings are ignored.
 * 
 * Generation of indexFiles
 *   - translateIndex = the indexfile
 * 
 * 
 * 
 * 

 */
Packer = function(cfg)
{
    
    XObject.extend(this, cfg);
    var _this = this;
    if (this.srcfiles && this.srcfiles.length) {
        this.srcfiles.forEach(function(f) {
            _this.loadSourceFile(f);
        });
        
    }
    
    if (!this.files) {
        throw "No Files";
    }
    
    var link = false;
    if (cfg.autoBuild) {
        
        function dateString(d){
            function pad(n){return n<10 ? '0'+n : n}
            return d.getFullYear() +
                 pad(d.getMonth()+1)+
                 pad(d.getDate())+'_'+
                 pad(d.getHours())+
                 pad(d.getMinutes())+
                 pad(d.getSeconds());
        }

        
        
        var version = 0;
        this.files.forEach(function(f) {
            version = Math.max(File.mtime(f), version);
        });
        var version  = dateString(new Date(version));
        
        var dirname = GLib.path_get_dirname(this.files[0]);
        var outname = this.module ? this.module : GLib.path_get_basename(dirname);
        this.target = dirname + '/compiled/' + outname + '-' + version + '.js';
         if (File.exists(this.target)) {
            print("Target file already exists: " + this.target);
            Seed.quit();
        }
        this.prefix = dirname +'/';
        this.translateJSON  = dirname + '/compiled/_translation_.js';
        
    }
     
    print(this.translateJSON);
    this.timer =  new Date() * 1;
    this.packAll();
    
    
    
 
}
Packer.prototype = {
    /**
     * @prop srcfiles {String} file containing a list of files/or classes to use.
     */
    srcfile : false,
    
    /**
     * @prop files {Array} list of files to compress (must be full path)
     */
    files : false,
    /**
     * @prop target {String} target to write files to - must be full path.
     */
    target : '',
    /**
     * @prop debugTarget {String} target to write files debug version to (uncompacted)- must be full path.
     */
    debugTarget : '', // merged file without compression.
    /**
     * @prop tmpDir {String} (optional) where to put the temporary files. 
     *      if you set this, then files will not be cleaned up
     */
    tmpDir : '/tmp',
    
    translateJSON : '', // json based list of strings in all files.
   
    /**
     * @prop cleanup {Boolean} (optional) clean up temp files after done - 
     *    Defaults to false if you set tmpDir, otherwise true.
     */
    cleanup : true,  
    
    /**
     * @prop prefix {String} (optional) prefix of directory to be stripped of when
     *    Calculating md5 of filename 
     */
    prefix : '',  
    out : '', // if no target is specified - then this will contain the result
    
    
    loadSourceFile : function(srcfile)
    {
        var lines = File.read(srcfile).split("\n");
        var _this = this;
        lines.forEach(function(f) {
            
            if (/^\s*\//.test(f) || !/[a-z]+/i.test(f)) { // skip comments..
                return;
            }
            if (/\.js$/.test(f)) {
                _this.files.push( f);
                // js file..
                return;
            }
            
            //println("ADD"+ f.replace(/\./g, '/'));
            var add = f.replace(/\./g, '/').replace(/\s+/g,'')+'.js';
            if (_this.files.indexOf(f) > -1) {
                return;
            }
            _this.files.push( add );
            
        })
    },
    
    
    packAll : function()  // do the packing (run from constructor)
    {
        
        //this.transOrigFile= bpath + '/../lang.en.js'; // needs better naming...
        //File.write(this.transfile, "");
        if (this.target) {
            File.write(this.target, "");
        }
        
        if (this.debugTarget) {
            File.write(this.debugTarget, "");
        }
        
        for(var i=0; i < this.files.length; i++)  {
            var file = this.files[i];
            
            print("reading " +file );
            if (!File.isFile(file)) {
                print("SKIP (is not a file) " + file);
                continue;
            }
           
            if (this.debugTarget) {
                File.append(this.debugTarget, File.read(file));
            }
            // it's a good idea to check with 0 compression to see if the code can parse!!
            
            // debug file..
            //File.append(dout, str +"\n"); 
            
       
            
            var minfile = this.tmpDir + '/' +file.replace(/\//g, '.');
            
            
            // let's see if we have a min file already?
            // this might happen if tmpDir is set .. 
            if (true && File.exists(minfile)) {
                var mt = File.mtime(minfile);
                var ot = File.mtime(file);
                print("compare : " + mt + "=>" + ot);
                if (mt >= ot) {
                    continue;
                    /*
                    // then the min'files time is > than original..
                    var str = File.read(minfile);
                    print("using MIN FILE  "+ minfile);
                    if (str.length) {
                        File.append(outpath, str + "\n");
                    }
                    
                    continue;
                    */
                }
                
            }
            
            print("COMPRESSING ");
            //var codeComp = pack(str, 10, 0, 0);
            if (File.exists(minfile)) {
                File.remove(minfile);
            }
            var str = File.read(file);
            var str = this.pack(str, file, minfile);
            if (str.length) {
                File.write(minfile, str);
            }
            
             
          
        }  
        if (this.translateJSON) {
            
               
            print("MERGING LANGUAGE");
            var out = "if (typeof(_T) == 'undefined') { _T={};}\n"
            if (this.target) {
                File.write(this.target, out);
            } else {
                this.out += out;
            }
            
            
            
            File.write(this.translateJSON, "");
            for(var i=0; i < this.files.length; i++)  {
                var file = this.files[i];
                var transfile= this.tmpDir + '/' +file.replace(/\//g, '.') +'.lang.trans';
                var transmd5 = this.tmpDir  + '/' +file.replace(/\//g, '.') +'.lang';
                if (File.exists(transmd5)) {
                    var str = File.read(transmd5);
                    if (str.length) {
                        if (this.target) {
                            File.append(this.target, str + "\n");
                        } else {
                            this.out += str + "\n";
                        }
                        
                    }
                    if (this.cleanup) {
                        File.remove(transmd5);
                    }
                }
                if (File.exists(transfile)) {
                    var str = File.read(transfile);
                    if (str.length) {
                        File.append(this.translateJSON, str);
                    }
                    if (this.cleanup) {
                        File.remove(transfile);
                    }
                }
                
               
            }
        }
        
        print("MERGING SOURCE");
        
        for(var i=0; i < this.files.length; i++)  {
            var file = this.files[i];
            var minfile = this.tmpDir + '/' + file.replace(/\//g, '.');
            
            
            if (!File.exists(minfile)) {
                continue;
            }
            var str = File.read(minfile);
            print("using MIN FILE  "+ minfile);
            if (str.length) {
                if (this.target) {
                    File.append(this.target, str + "\n");   
                } else {
                    this.out += str + "\n";
                }
                
            }
            if (this.cleanup) {
                File.remove(minfile);
            }
            
        }
        
         
    
    
    },
    /**
     * Core packing routine  for a file
     * 
     * @param str - str source text..
     * @param fn - filename (for reference?)
     * @param minfile - min file location...
     * 
     */
    
    pack : function (str,fn,minfile)
    {
    
        var tr = new  TokenReader(  { 
            keepDocs :true, 
            keepWhite : true,  
            keepComments : true, 
            sepIdents : true,
            collapseWhite : false
        });
        this.timerPrint("START" + fn);
        
        // we can load translation map here...
        
        var toks = tr.tokenize(new TextStream(str)); // dont merge xxx + . + yyyy etc.
        
        // at this point we can write a language file...
        if (this.translateJSON) {
            
            this.writeTranslateFile(fn, minfile, toks);
        }
        
        this.activeFile = fn;
        
        // and replace if we are generating a different language..
        
        this.timerPrint("Tokenized");
        //var ts = new TokenStream(toks);
        //print(JSON.stringify(toks, null,4 )); Seed.quit();
        var ts = new Collapse(toks);
       // print(JSON.stringify(ts.tokens, null,4 )); Seed.quit();
        //return;//
        var sp = new ScopeParser(ts);
        this.timerPrint("Converted to Parser");
        sp.packer = this;
        sp.buildSymbolTree();
        this.timerPrint("Built Sym tree");
        sp.mungeSymboltree();
        this.timerPrint("Munged Sym tree");
        print(sp.warnings.join("\n"));
        
        
        //var out = CompressWhite(new TokenStream(toks), this, true); // do not kill whitespace..
        var out = CompressWhite(new TokenStream(toks), this, false);
        this.timerPrint("Compressed");
        return out;
        
        
         
    },
    
    timerPrint: function (str) {
        var ntime = new Date() * 1;
        var tdif =  ntime -this.timer;
        this.timer = ntime;
        print('['+tdif+']'+str);
    },
    
    /**
     * 
     * Translation concept...
     * -> replace text strings with _T....
     * -> this file will need inserting at the start of the application....
     * -> we need to generate 2 files, 
     * -> a reference used to do the translation, and the _T file..
     * 
     */
    
    writeTranslateFile : function(fn, minfile, toks) 
    {
        
        var map = {};
        var _this = this;
        toks.forEach(function (t) {
            if (t.type == 'STRN' && t.name == 'DOUBLE_QUOTE') {
                var sval = t.data.substring(1,t.data.length-1);
                var ffn = fn.substring(_this.prefix.length);
                map[sval] = _this.md5(ffn + '-' + sval);
            }
        })
        
        var transfile = minfile + '.lang.trans';
        var transmd5 = minfile + '.lang';
        print("writeTranslateFile "  + transfile);
        var i = 0;
        var v = '';
        if (File.exists(transfile)) {
            File.remove(transfile);
        }
        if (File.exists(transmd5)) {
            File.remove(transmd5);
        }
        for(v in map) { i++; break };
        if (!i ) {
            return; // no strings in file...
        }
        var ffn = fn.substring(this.prefix.length);
         
         
        File.write(transfile, "\n'" + ffn  + "' : {");
        var l = '';
        var _tout = {}
         
        File.write(transmd5, '');
        for(v in map) {
            File.append(transfile, l + "\n\t \"" + v + '" : "' + v + '"');
            l = ',';
            // strings are raw... - as the where encoded to start with!!!
            File.append(transmd5, '_T["' + this.md5(ffn + '-' + v) + '"]="'+v+"\";\n");
        }
        File.append(transfile, "\n},"); // always one trailing..
        
         
    },
    md5 : function (string)
    {
        
        return GLib.compute_checksum_for_string(GLib.ChecksumType.MD5, string, string.length);
        
    },
    stringHandler : function(tok)
    {
        //print("STRING HANDLER");
       // callback when outputing compressed file, 
       var data = tok.data;
        if (!this.translateJSON) {
         //   print("TURNED OFF");
            return data;
        }
        if (tok.name == 'SINGLE_QUOTE') {
            return data;
        }
        
        var sval = data.substring(1,data.length-1);
        // we do not clean up... quoting here!??!!?!?!?!?
        
        
        // blank with tabs or spaces..
        //if (!sval.replace(new RegExp("(\\\\n|\\\\t| )+",'g'), '').length) {
       //     return tok.outData;
       // }
        
        var sval = tok.data.substring(1,data.length-1);
        var fn = this.activeFile.substring(this.prefix.length);
        
        
        return '_T["' + this.md5(fn + '-' + sval) + '"]';
        
        
    }
    
    
};
