
// <script type="text/javascript">

TokenReader = imports.TokenReader.TokenReader;
ScopeParser= imports.ScopeParser.ScopeParser;
Collapse = imports.Collapse.Collapse;
TextStream = imports.TextStream.TextStream;
 

function  escapeHTML(str) { 
    return str.replace(/&/g,'&amp;').
            replace(/>/g,'&gt;'). 
            replace(/</g,'&lt;'). 
            replace(/"/g,'&quot;');
};

function toPretty(str)
{
    
    var txs = new TextStream(str);
    var tr = new TokenReader({ keepComments : true, keepWhite : true });
    var toks = tr.tokenize(txs)
    
    //var sp = new ScopeParser(new Collapse(toks));
    //sp.buildSymbolTree();
    
    
   // sp.mungeSymboltree();
    var r = '';
    //r += sp.warnings.join("<BR>");
    //r == "<BR>";
    
    
    
    
    var cs = ''; // current style..
    
    function toStyle(tok)
    {
        if (tok.is("WHIT") || tok.is("COMM") ) {
            if (tok.data.indexOf("/") > -1) {
                return 'comment';
            }
            return cs; // keep the same..
        }
        if (tok.is('STRN')) {
            return 'string';
        }
        // other 'vary things??
        if (tok.is('NAME') || tok.data == '.' || tok.name == 'THIS') {
            return 'var';
        }
        if (/^[a-z]+/i.test(tok.data)) {
            return 'keyword';
        }
        return 'syntax'
    }
    // loop through and print it...?
    
    
    for (var i = 0;i < toks.length; i++) {
        var ns = toStyle(toks[i]);
        if (ns != cs) {
            // change of style
            if (cs.length) r +='</span>';
            r +='<span class="jsdoc-'+ns+'">';
            cs = ns;
        }
        if (toks[i].identifier) {
            
            r += '<span class="with-ident2">' +
                escapeHTML(toks[i].data) + '</span>';
                continue;
                
        }
        r += escapeHTML(toks[i].data); //.replace(/\n/g, "<BR/>\n");
    }
    if (cs.length) r +='</span>';
    
    return '<code class="jsdoc-pretty">'+r+'</code>';
    
        
}
