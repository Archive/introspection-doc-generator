//<script type="text/javscript">

XObject = imports.XObject.XObject;
console     = imports.console.console;
TokenStream = imports.TokenStream.TokenStream;
/**
 * 
 * this takes our collased file, and turns it into the config array..
 * 
 * 
 * 
 * 
 * 
 * 
 */

RooFile  =  XObject.define(
    function (ar)
    {
        RooFile.superclass.constructor.call(this, ar);
       // console.log("STARTING OUTPUT");
        
         

    },
    TokenStream, 
    {
        
        cfg : { },

        parse: function()
        {
            
            if (this.tokens[0].data == 'Pman.on') {
                // then it's a layout style..
                
                //Seed.quit();
                
                
                //console.dump(this.tokens);
                //              the list of statements inside of function..?
                var stmts =  this.tokens[1].items[1][2].items;
                // look for register..
                var topp = false;
                stmts.map( function(s, i) {
                    if (s[0].data == 'Pman.register') {
                        topp = brace = s[1].items[0][0].props;
                    }
                });
                if (!topp) {
                    console.dump(this.tokens);
                    throw "could not find top props...";
                    
                }
                
                this.cfg = this.parseProps(topp);
                this.cfg.name = this.tokens[3].data;
                
                
                //                  (            {          add    { this.panel (  {
                var cfg = this.tokens[7].items[0][0].props.add.val[2].items[2][3].items[0][0].props;
                this.cfg.items = [ this.parseProps(cfg) ];
               // console.dump(this.cfg); 
                
                return;
            }
            
            if (this.tokens[2].props && typeof(this.tokens[2].props.create) != 'undefined') {
                //console.dump(this.tokens);
                this.cfg.name = this.tokens[0].data;
                
                
                
                var cfg = this.tokens[2].props.create.val[2].items[1][3].items[0][0].props;
                //console.dump(this.tokens);
                this.cfg.items = [ this.parseProps(cfg) ];
                
            }
            // perfect for dialogs...
            
            
            var pos = this.lookFor( 'function');
            
            // XXXXX = function(
            var cfg = {};
            
            if (pos > -1 && (this.tokens[pos-1].data == '=') && (this.tokens[pos-2].type == 'NAME')) {
                
                this.cfg = {
                    '*class' : this.tokens[pos-2].data,
                    '*extends' : '',
                    '//constructor' :  this.tokens[pos-2].prefix,
                    '|constructor' : 'function ' + this.tokens[pos+1].toRaw() + 
                            this.tokens[pos+2].toRaw(),
                    
                };
                this.cursor = pos+2;
                
            }
            
            if (pos < 0) {
                // no function - we have a static one...
                pos = this.lookFor( '=');
                if (pos > 1 && 
                    (this.tokens[pos-1].type == 'NAME') &&
                    (this.tokens[pos+1].data == '{')
                ) {
                    this.cfg = {
                        '*class' : this.tokens[pos-1].data,
                        '//*class' :  this.tokens[pos-1].prefix,
                        
                        
                    };
                    XObject.extend(this.cfg, this.parseProps(this.tokens[pos+1].props));
                    return;
                    
                }
                
                
                
            }
            
            
              // Roo.apply (l
            var pos = this.lookFor( 'Roo.apply');
            //console.dump(this.tokens);
            
            
            if (
                (pos > -1) &&
                (this.tokens[pos+1].items[0][0].data  == this.cfg['*class'] + '.prototype')
            ) {
                // XXXXXx.prototype = {
                
                
                    
                XObject.extend(this.cfg, this.parseProps(this.tokens[pos+1].items[1][0].props));
                return;
                
            }
            
            
            var pos = this.lookFor('new');
            
            if (pos > -1 && 
                (this.tokens[pos-2].type == 'NAME') &&
                (this.tokens[pos-1].data == '=') && 
                (this.tokens[pos+1].type == 'NAME') &&
                (this.tokens[pos+2].data == '(') 
            ) {
                
                this.cfg = {
                    '//*class' : this.tokens[pos-2].prefix,
                    '*class' : this.tokens[pos-2].data,
                    '*extends' : this.tokens[pos+1].data
                };
                    
                XObject.extend(this.cfg, this.parseProps(this.tokens[pos+2].items[0][0].props));
                
                return;
                
            }
            
            ///Builder.Provider.ProjectManager = new Roo.Observable({
                
            
            
            var pos = this.lookFor( 'Roo.extend');
            if (pos > -1) {
                
                this.cfg['*extends'] = this.tokens[pos+1].items[1][0].data;
                XObject.extend(this.cfg, this.parseProps(this.tokens[pos+1].items[2][0].props));
                
                // no more..
                return;
            }
            
             
                
            var pos = this.lookFor( 'Roo.extend');
            if (pos > -1) {
                
                this.cfg['*extends'] = this.tokens[pos+1].items[1][0].data;
                XObject.extend(this.cfg, this.parseProps(this.tokens[pos+1].items[2][0].props));
                
                // no more..
                return;
            }
            
            
            
            //console.dump(cfg);
            //Seed.quit();
            
            
            
            // other type of layout
          //  console.dump(this.tokens);
            
            // this type of file..
            // xxxx.xxx = function
            // -> into |constructor..
            // -> extend + prototype
             
            
            
                
        },
        
        parseProps:  function(o)
        {
            var ret = { };
            var fakeItems =  [];
            for(var k in o) {
                
                
                if (o[k].key.data == '}') {
                    // typo trailing comma in object def.
                    continue;
                }
                
                var kv = k;
                if (o[k].key.type == 'STRN') {
                    kv = o[k].key.toJS();
                }
                if (!o[k].val.length) {
                    console.dump(o[k]);
                }
                //console.dump(o[k]);
                if (o[k].val[0].data == "function") {
                    // add comments..
                  //   console.dump(o[k].key.prefix); 
                    var pr = typeof(o[k].key.prefix) == 'undefined' ? '' : o[k].key.prefix ;
                    pr = this.clean_prefix( pr) ;
                    if (pr.length) {
                        ret['//' +kv ] =  pr;
                    }
                    
                    ret['|' +kv ] =  this.clean_body( this.expand(o[k].val));
                    continue;
                }
                if (o[k].val[0].data == "[") {
                    
                    if (o[k].val[0].items[0][0].data == '{') {
                        // array of objects..
                        
                        // this works for items..
                        
                        // used elsewhere for buttons: -> in which case we have a fake xtype
                        
                        
                        
                        // if K is not ITEMS - we need to add '*prop' -> and add it to the items array..
                        var add = this.parseArray(o[k].val[0].items);
                        if (kv == 'items') {
                            ret[kv] = add;
                            continue;
                        }
                        var fake_array = {
                            xtype : 'Array.' + kv,
                            '*prop' : kv,
                            items : add
                        }
                        fakeItems.push(fake_array);
                        continue;
                    } 
                    // raw array 
                    
                    
                    ret['|' +kv ] =  this.clean_body(this.expand(o[k].val)); // remove ','...
                    continue;
                }
                if (o[k].val[0].data == "(") {
                    ret['|' +kv ] =  this.expand(o[k].val);
                    continue;
                }
                if (o[k].val[0].data == "{") {
                    
                    // things that can be property of object type:
                    // listeners, set, 
                    var add = this.parseProps(o[k].val[0].props);
                    if (kv == 'set' || kv =='listeners') {
                        ret[kv ] = add;
                        continue;
                    }
                    if ((typeof(add.xtype) != 'undefined') ||  ['sortInfo', 'center', 'east', 'west', 'north', 'south'].indexOf(kv) > -1) {
                        add['*prop'] =  kv;
                        fakeItems.push(add);
                        continue;
                    }
                    
                    
                    ret[ '|' + kv ] =  this.expand(o[k].val);
                    
                    
                    // this hsould be added to 'items', with a *prop element..
                    continue;
                }
                //console.dump(o[k].val);
                
                if (o[k].val[1].data == ',' || o[k].val[1].data == '}') {
                    // single item piece of data...
                    var t1= o[k].val[0];
                    switch(o[k].val[0].type) {
                        case 'STRN':
                        case 'NUMB':
                        case 'KEYW':
                            ret[  kv ]  = t1.toJS();
                            continue;
                        case 'NAME':
                            ret[ '|' + kv ] =  t1.data;
                            continue;
                        
                    }
                }
               
                ret[ '|' + kv ] =  this.expand(o[k].val);
                
            }
            if (!ret.items && fakeItems.length) {
                ret.items = [];
            }
            XObject.extend(fakeItems, function(e) {
                ret.items.push(e);
            })
            // merge fakeItems;
            //console.dump(ret);
            
            return ret;
        },
        parseArray: function(ar) {
          // console.dump(ar);
           // Seed.quit();
            var ret = [];
            ar.map(function (e) {
                // has no props...
                if (typeof(e[0].props) == 'undefined') {
                    return;
                }
               
                
                ret.push( this.parseProps(e[0].props));
                     
            },this);
             
            return ret;
          
        },
        
        
        expand: function(ar)
        {
            var ret = '';
           
            
            for(var i =0 ; i < ar.length -1; i++) {
                ret += ar[i].toRaw();
            }
            
           
           
            return ret;
            
            
        },
        clean_body : function(str)
        {
            var lns = str.split("\n");
            var mindent = -1;
            lns.map( function(l, i) {
                if (!i || !l.length || l.match(/^\s+$/)) {
                    return;
                }
                
                var spc = l.match(/\s+/);
                if (!spc || !spc[0].length) {
                    return;
                }
                mindent = mindent < 0 ? spc[0].length : Math.min(spc[0].length, mindent);
                
            });
            //console.log(mindent + ":" + lns[0]);
            var ar = [];
            if (mindent < 0) {
                return str;
            }
            lns.map( function(l,i) {
                if (!i) {
                    ar.push(l.replace(/^\s+/, ''));
                    return;
                }
                ar.push(l.substring(mindent));
            });
            return ar.join("\n");
        },
        clean_prefix: function(str) {
            
            
            
            if (!str.length) {
                return '';
            }
            var ret = str.replace(/^\s+/gm, "\n").replace(/\n+/gm, "\n");
            return ret == "\n" ? '' : ret;
            
        }    
             
    
});