//<Script type="text/javascript">

XObject = imports.XObject.XObject;

DocTag = imports.DocTag.DocTag;

/**
 * Create a new DocComment. This takes a raw documentation comment,
 * and wraps it in useful accessors.
 * @class Represents a documentation comment object.
 * 
 */ 
 

DocComment = XObject.define(

    function(/**String*/comment) {
        this.isUserComment = true;
        this.src           = "";
        this.meta          = "";
        this.tagTexts      = [];
        this.tags          = []; // array of doctags..
        if (typeof comment != "undefined") {
            this.parse(comment);
        }
    },

    Object, // extends

    {
        isUserComment : true,
        src           : "",
        meta          : "",
        tagTexts      : [],
        tags          : [],     
    
        /**
         * serialize..
         */
        toJSON :function(t)
        {
            
            var ret = { '*object' : 'DocComment' };
            
            var _this = this;
            ['isUserComment','src', 'meta',  'tags'].forEach(function(k) {
                ret[k] = _this[k];
            })
            
            return ret;
        },    
        /**
        * @requires JSDOC.DocTag
        */
        parse : function(/**String*/comment) {
            if (comment == "") {
                comment = "/** @desc */";
                this.isUserComment = false;
                
            }
            
            this.src = DocComment.unwrapComment(comment);
            
            //println(this.src);
            
            
            this.meta = "";
            if (this.src.indexOf("#") == 0) {
                this.src.match(/#(.+[+-])([\s\S]*)$/);
                if (RegExp.$1) this.meta = RegExp.$1;
                if (RegExp.$2) this.src = RegExp.$2;
            }
            this.hasTags = true;
            if (!/^\s*@\s*\S+/m.test(this.src)) {
                this.isUserComment = false;
                this.hasTags = false;
                
                //return;
            }
            this.fixDesc();
            
            //if (typeof JSDOC.PluginManager != "undefined") {
            //    JSDOC.PluginManager.run("onDocCommentSrc", this);
            //}
            
            this.src = DocComment.shared+"\n"+this.src;
            this.tags = [];
            this.tagTexts = [];
            
            
           
            this.tagTexts = 
                this.src
                .split(/(^|[\r\n])\s*@/)
                .filter(function($){return $.match(/\S/)});
            
            //println(this.tagTexts.toSource());
            // fix tagText
            
            
            
            /**
                The tags found in the comment.
                @type JSDOC.DocTag[]
             */
             
            this.tags = this.tagTexts.map(function($){return new DocTag($)});
            
            //println(this.tags.toSource());
            this.tagTexts = []; // we dont need to store this..
            
            
            //if (typeof JSDOC.PluginManager != "undefined") {
            //     JSDOC.PluginManager.run("onDocCommentTags", this);
            //}
        },
         

        /**
            If no @desc tag is provided, this function will add it.
         */
        fixDesc : function() {
            if (this.meta && this.meta != "@+") return;
            
            
            
            // does not have any @ lines..
            // -- skip comments without @!!
            if (!/^\s*@\s*\S+/.test(this.src)) {
                this.src = "@desc "+this.src;
                // TAGS that are not \n prefixed!! ...
                this.src = this.src.replace(/@\s*type/g, '\n@type'); 
            
                return;
            }
            // kdludge for stuff...
            //this.src = this.src.replace(/@\s*type/g, '\n@type'); 
            
            // only apply @desc fix to classes..
            if (!/\s*@(class|event|property)/m.test(this.src) ) {
                return;
            }
            // if no desc - add it on the first line that is not a @
            var lines = this.src.split("\n");
            var nsrc = '';
            var gotf = false;
            
            for(var i =0; i < lines.length;i++) {
                var line = lines[i];
                if (gotf) {
                    nsrc += line + "\n";
                    continue;
                }
                if (/^\s*[@\s]/.test(line)) { // line with @
                    nsrc += line + "\n";
                    continue;
                }
                gotf = true;
                nsrc += '@desc ' + line + "\n";
                
            }
             
            this.src = nsrc;
            
            
            
        },
      
    /**
        Provides a printable version of the comment.
        @type String
     */
        toString : function() {
            return this.src;
        },

    /*~t
        assert("testing JSDOC.DocComment#fixDesc");
        var com = new JSDOC.DocComment();
        com.src = "foo";
        assertEqual(""+com, "foo", "stringifying a comment returns the unwrapped src.");
    */

    /**
        Given the title of a tag, returns all tags that have that title.
        @type JSDOC.DocTag[]
     */
     /*
     
        toQDump : function(t)
        {
            //println(t.toSource());
            var r =  JSDOC.toQDump(t, 'JSDOC.DocComment.fromDump({', '})', {}); // send it an empty object..
            //println(r);
            return r;
        } ,
        */
     
        getTag : function(/**String*/tagTitle) {
            return this.tags.filter(function($){return (typeof($['title']) != 'undefined') && ($.title == tagTitle)});
        }
        
});


/// static methods..

XObject.extend(DocComment,
{
        
        /**
         * Used to store the currently shared tag text.
         */
        shared : "",
        
        /**
         * Remove slash-star comment wrapper from a raw comment string.
         *  @type String
         */
        unwrapComment : function(/**String*/comment) {
            if (!comment) return "";
            var unwrapped = comment.replace(/(^\/\*\*|\*\/$)/g, "").replace(/^\s*\* ?/gm, "");
            return unwrapped;
        },

        fromDump : function(t)
        {
            var ns = new DocComment();
            for (var i in t) {
                ns[i] = t[i];
            }
            return ns;
        }
});
