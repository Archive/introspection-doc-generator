//<script type="text/javascript">
/**
 * Options management...
 */
XObject = imports.XObject.XObject;
File = imports.File.File;

Options = {

    // generic stuff...
    "--help"          : 'Show help',   // was h
    "help": false,   // was h;se
    
    
    // configurable settings.. - 
    "usage" : "Usage seed jsdocbuild.js OPTIONS \n",
    
    
    // options get defined like this..
    "--src"           :  "source directory (either absolute - starts with "/" or relative " + 
                        "- without, in which case it's added to baseDir",
    "--exclude-src"       : 'Ex',   
    "--baseDir"       :  'Base Directory (root directory of code)',
    "--target"        :  'Target Directory (where html files go)',
    "--cacheDirectory": 'Cached Files Directory (or blank to not cache)',
    "--conf"          : 'Read From a Configuration file',       // was c. - configuration file.. - parsed with JSON.parse
    "--templateDir"      : 'Template Directory',   // was t.
    // "recurse": false,   // was r. - not supported..
    "--ext"           :  'Extension of code files to read (normally js)',   // was x.
    "--publishExt"    : 'Extension of html files to write (normally html)',
    //"private": '',   // was p
    //"allfunctions": '',   // was a
    //"encoding": '',   // was e.
    //"nocode": '',  // was n
    //"out": '',   // was o.
    //"suppress": '',  // was s ??? used?
    "--outputSource" : 'Output the Source code to symbols/src/* (boolean)',
    //"testmode": '',  // was t
    
    "--verbose"       : 'Show verbose messages',   // was v
    //"disablecache": '',   // was C -- not needed? - see if cacheDirectory was set..
    //"define" : [],   // was D.
    //"handler" : [],  // was H -- not supported..

    
    // and now the defaults.. (which type can be infered from..)
    "src" : [],
    "exclude-src" : [],
    "baseDir" :  '',  // base directory - 
    "target" : '',   // was d. ?? source directory (needed to put temporary files..)
    "cacheDirectory" : '',
    "conf" : '',       // was c. - configuration file.. - parsed with JSON.parse
    "templateDir": '',   // was t.
    // "recurse": false,   // was r. - not supported..
    "ext": 'js',   // was x.
    "publishExt" : 'html',
    "private": '',   // was p
    "allfunctions": '',   // was a
    "encoding": '',   // was e.
    "nocode": '',  // was n
    "out": '',   // was o.
    "suppress": '',  // was s ??? used?
    "outputSource" : true,
    "testmode": '',  // was t
    
    "verbose": '',   // was v
    "disablecache": '',   // was C
    "define" : [],   // was D.
    "handler" : [],  // was H -- not supported..
    
    
    "version" : "1.0",
    "copyright" : "LGPL",
    
    LOG : {
        warn : function(str) {
            print("Warn: " +str );
        },
        inform : function(str) {
            print("Inform: " +str );
        },
        close : function() { },
        flush : function() { },
        out: false,
        warnings : [],
        verbose : false    
    },
    init : function()
    {
        
        if (this.help) {
            this.showHelp();
          
        }
        
        // the reset of this is specific to JSDOC - and can not be moved to a generic handler..
        
         
        this.LOG.verbose = this.verbose;
        
        if (!this.baseDir) { // should we set this to cwd?
            throw {
                name: "ArgumentError", 
                message: "No baseDir specified" 
            };
        }
        
        // this is most likely to come from the command line..
        if (this.conf) {
            var conf = this.conf[0] == '/' ? this.conf : this.baseDir + '/' + this.conf;
        
            XObject.extend(this, JSON.parse(File.read(conf)));;
        }
        // help ?? -- usage..
       
        if (!this.src.length) {
            throw {
                name: "ArgumentError", 
                message: "No source directories specified" 
            };
        }
        // append full path to source directories.
        var _this= this;
        var src = this.src;
        this.src = [];
        src.forEach(function(v, i) {
            if (!v.length || v[0] != '/') {
                v = _this.baseDir + (v.length ?  '/' + v : '');
            }
            if (!File.exists(v)) {
                throw {
                    name: "ArgumentError", 
                    message: "invalid Source Directory : " +  v
                };
            }
            _this.src.push(v);
        });
        
        
        if (!this.templateDir) {
            throw {
                name: "ArgumentError", 
                message: "No templateDir Directory specified" 
            };
        }
        if (this.templateDir[0] !='/') {
            this.templateDir = this.baseDir + '/' + this.templateDir;
        }
        
        
        if (!this.target) {
            throw {
                name: "ArgumentError", 
                message: "No directory specified" 
            };
        }
        
        //print(JSON.stringify(this, null,4));
        
        // should cacheDirectory be a subdirectory of target??
        // if not set..
        //if (!this.cacheDirectory) {
        //    throw {
        //        name: "ArgumentError", 
        //        message: "No cacheDirectory specified" 
        //    };
        // }
        
    },
    /** 
     *  this might be nice as a standard bit of code..
     */
       
    parseArgv : function() 
    {
        
        var args = Array.prototype.slice.call(Seed.argv);
        args.shift(); //seed
        args.shift(); // pack.js
        
        for(var i =0; i < args.length;i++) {
            if (args[i].substring(0,2) != '--') {
                
                throw {
                    name: "ArgumentError", 
                    message: "Unknown argument: " + args[i] 
                };
            }
            var a = args[i].substring(2);
            if (typeof(this[args[i]]) == 'undefined') {
                throw {
                    name: "ArgumentError", 
                    message: "Unknown argument: " + args[i] 
                };
            }
            // type!!?!?
            if (typeof(this[a]) == 'string') {
                this[a] = args[i+1];
                i++;
                continue;
            }
            if (typeof(this[a]) == 'boolean') {
                if (['false', 'true'].indexOf(args[i+1]) < 0) {
                    throw {
                        name: "ArgumentError", 
                        message: "Unknown value for : " + args[i] + ' : ' +  args[i+1] 
                    };
                }
                this[a] = args[i+1] == 'true';
                i++;
                continue;
            }
            if (typeof(this[a]) == 'object') { // tecnically an array.
                i++;
                while(i < args.length)
                {
                    if (args[i].substring(0,2) == '--'){
                        i--;
                        break;
                    }
                    this[a].push(args[i]);
                    i++;
                }
                
                continue;
            }
            throw {
                name: "ArgumentError", 
                message: "Do not know how to handle: " + a + ' ' + typeof(this[a])
            };  
        }
        
        
    },
    
    
    showHelp: function()
    {
        print(this.usage);
        for(var i in this) {
            if (i.substring(0,2) != '--') {
                continue;
            }
            print( i + '  ARG  : ' + this[i]);
            throw "DONE";
        }
    }
}