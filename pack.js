#!/usr/bin/seed
//<script type="text/javascript">
/**
 * packer command line
 * 
 * -o Output
 * -O Output debug file here.
 * -t Translate json file.
 * -w Cache / working dir.
 * -f File to read with a list of source paths / or class names.
 * -C no cleanup (use with -w if you need are using a cache directory.)
 * -p prefix for translation md5 generator (directory that files are in, and is removed 
 *    from path when generating an md5 for the translated name.
 * compresses files listed as arguments and outputs result
 */
 
File            = imports.File.File;
Packer          = imports.JSDOC.Packer.Packer;


 
  

var args = Array.prototype.slice.call(Seed.argv);
args.shift(); //seed
args.shift(); // pack.js
var cfg = {
    files : [],
    target : false,
    srcfiles : []
}


for(var i =0; i < args.length;i++) {
    if (args[i] == '-o') {
        cfg.target = args[i+1];
        i++;
        continue;
    }
    if (args[i] == '-O') {
        cfg.debugTarget = args[i+1];
        i++;
        continue;
    }
    if (args[i] == '-t') {
        cfg.translateJSON = args[i+1];
        i++;
        continue;
    }
    if (args[i] == '-w') {
        cfg.tmpDir = args[i+1];
        i++;
        continue;
    }
    if (args[i] == '-p') {
        cfg.prefix = args[i+1];
        i++;
        continue;
    }
 
    if (args[i] == '-C') {
        cfg.cleanup = false;
        continue;
    }
    if (args[i] == '-f') {
        cfg.srcfiles.push(args[i+1]);
        i++;
        continue;
    }
    if (cfg.files.indexOf(args[i]) > -1) {
        continue; // remove dupes.
    }
    cfg.files.push(args[i]);
}
var pack;
try {
    pack = new Packer(cfg)
} catch (e) {
    print("ERROR " + e.toString());
    throw e;
}
if (!pack.target) {
    print(pack.out);
}


