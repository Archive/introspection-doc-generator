docs generator for gobject-introspection

Requires seed

run:
seed docs.js /var/www

outputs to /var/www/seed
** if you have templates for other languages they will be output into /var/www/LANG 

----------------------------
Licence:
JSDOC code based on http://code.google.com/p/jsdoc-toolkit/ : MIT Licence
Introspection - LGPL 

** FIXME - put the licence headers in the code.!
---------------------------

TODO list:
- Work out how to host the bugger..
- More libraries (related to gobject-introspection hacking)
- Better documentation - we probably have to manually generate GIR's from source for this, as we have a 'special' doc requirement
- add gir builder script (chroot/jhbuild..)


