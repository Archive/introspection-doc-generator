//<script type="text/javascript">

XObject  = imports.XObject.XObject;

/**
 * Generic Template Link handler..
 * 
 * 
 * 
 */

/** Handle the creation of HTML links to documented symbols.
	@constructor
*/
Link = XObject.define(
    /*
    * constructor
    */ 
    function (opts) {
        XObject.extend(this,opts);
        
    }, 
    Object,
    {
        
         /**
         * url {String} url for link..
         */
        url: "",
        /**
         * text {String} text to show on link.
         */
        
        text : "",
        
        /**
         * alias {String} not sure?
         */
        alias : "",
        /**
         * src {String} not sure?
         */
        src : "",
        file : "",
        
        innerName : "",
        classLink : false,
        targetName : "",
    
        
        
        target : function(targetName) {
            if (typeof(targetName) != 'undefined') this.targetName = targetName;
            return this;
        },
        inner : function(inner) {
            if (typeof(inner) != 'undefined') this.innerName = inner;
            return this;
        },
        withText : function(text) {
            if (typeof(text) != 'undefined') this.text = text;
            return this;
        },
        toSrc : function(filename) {
            if (typeof(filename) != 'undefined') this.src = filename;
            
            return this;
        },
        toSymbol : function(alias) {
            if (typeof(alias) != 'undefined') {
                this.alias = new String(alias);
            }
            return this;
        },
        toClass : function(alias) {
            this.classLink = true;
            return this.toSymbol(alias);
        },
        toFile : function(file) {
            if (typeof(file) != 'undefined') this.file = file;
            return this;
        },
        
        toString : function() {
    
            var thisLink = this;

            if (this.alias) {
                return  this.alias.replace(/(^|[^a-z$0-9_#.:-])([|a-z$0-9_#.:-]+)($|[^a-z$0-9_#.:-])/i,
                    function(match, prematch, symbolName, postmatch) {
                        var symbolNames = symbolName.split("|");
                        var links = [];
                        for (var i = 0, l = symbolNames.length; i < l; i++) {
                            thisLink.alias = symbolNames[i];
                            links.push(thisLink._makeSymbolLink(symbolNames[i]));
                        }
                        return prematch+links.join("|")+postmatch;
                    }
                );
            }
            if (this.url) {
                return thisLink._makeLink(this.url);
            }
            if (this.src) {
                return thisLink._makeSrcLink(this.src);
            }
            if (this.file) {
                return thisLink._makeFileLink(this.file);
            }

        },
        
        
        
        
        
        
        
        /** Create a link to a snother symbol. */
        _makeSymbolLink : function(alias) 
        {
            //print(JSON.stringify(alias));
            // look for '/' in alias..
            if (/\//.test(alias)) {
                var bits = alias.split('/');
                var ret = "";
                for(var i=0; i < bits.length; i++) {
                    if (i > 0) {
                        ret +="/";
                    }
                    ret += this._makeSymbolLink(bits[i]);
                }
                return ret;
                
            }
            
            
            
            var linkBase = './';
            var linkTo = Link.symbolSet.getSymbol(alias);
            
            var linkPath;
            var target = (this.targetName)? " target=\""+this.targetName+"\"" : "";
            
            // is it an internal link?
            if (alias.charAt(0) == "#") {
                linkPath = alias;
                fullLinkPath = alias;
            
            // if there is no symbol by that name just return the name unaltered
            } else if (!linkTo) {
                
                if (typeof(Link.builtins[alias]) != 'undefined') {
                    return "<a href=\""+ Link.builtins[alias]+"\""+target+">"+alias+"</a>";
                 }
                
                return this.text || alias;
            
            
            // it's a symbol in another file
            } else {

                if (!linkTo.is("CONSTRUCTOR") && !linkTo.isNamespace) { // it's a method or property
                    linkPath = escape(linkTo.memberOf) || "_global_";
                    linkPath += '.html#' + Link.symbolNameToLinkName(linkTo);
                }
                else {
                    linkPath = escape(linkTo.alias);
                    linkPath += '.html' + (this.classLink? "":"#" + Link.hashPrefix + "constructor");
                }
                //linkPath = linkBase + linkPath;
                fullLinkPath = linkBase + linkPath;
            }
            
            var linkText = this.text || alias;
            
            var link = {linkPath: linkPath, linkText: linkText, fullLinkPath: fullLinkPath};
            
            //if (typeof JSDOC.PluginManager != "undefined") {
            //    JSDOC.PluginManager.run("onSymbolLink", link);
            //}
            
            return "<a href=\""+link.fullLinkPath+"\""+target+" roo:cls=\""+link.linkPath+"\">"+link.linkText+"</a>";
        },


        /** Create a link to a source file. */
        _makeSrcLink : function(srcFilePath) {
            var target = (this.targetName)? " target=\""+this.targetName+"\"" : "";
                
            // transform filepath into a filename
            var srcFile = srcFilePath.replace(/\.\.?[\\\/]/g, "").replace(/[:\\\/]/g, "."); // was _
            var lsrcFile = srcFilePath.replace(/\.\.?[\\\/]/g, "").replace(/[:\\\/]/g, ".");
            var outFilePath = Link.base + '/symbols/' +  srcFile.replace(/.js$/, '') + 
                imports.JSDOC.Options.Options.publishExt;
            
            if (!this.text) this.text = srcFilePath; //FilePath.fileName(srcFilePath);
            return "<a href=\""+outFilePath+"\""+target+" roo:cls=\"src/"+lsrcFile+"\">"+this.text+"</a>";
        },

        /** Create a link to a source file. */
        _makeFileLink : function(filePath) {
            var target = (this.targetName)? " target=\""+this.targetName+"\"" : "";
                
            var outFilePath =  Link.base + filePath;

            if (!this.text) this.text = filePath;
            return "<a href=\""+outFilePath+"\""+target+">"+this.text+"</a>";
        },
        
          /** very basic link... */
        _makeLink : function(url) {
            var target = (this.targetName)? " target=\""+this.targetName+"\"" : "";
             
            if (!this.text) this.text = url;
            return "<a href=\""+url+"\""+target+">"+this.text+"</a>";
        }
        
});




/** prefixed for hashes */
Link.hashPrefix = "";

/** Appended to the front of relative link paths. */
Link.base = "";

Link.symbolNameToLinkName = function(symbol) {
	var linker = "";
	if (symbol.isStatic) linker = ".";
	else if (symbol.isInner) linker = "-";
	
	return Link.hashPrefix+linker+symbol.name;
}


Link.builtins = {
    'Object' : 'http://developer.mozilla.org/en/docs/Core_JavaScript_1.5_Reference:Global_Objects:Object',
    'Object...' : 'http://developer.mozilla.org/en/docs/Core_JavaScript_1.5_Reference:Global_Objects:Object',
    'Function' : 'http://developer.mozilla.org/en/docs/Core_JavaScript_1.5_Reference:Global_Objects:Function',
    'String' : 'http://developer.mozilla.org/en/docs/Core_JavaScript_1.5_Reference:Global_Objects:String',
    'Number' : 'http://developer.mozilla.org/en/docs/Core_JavaScript_1.5_Reference:Global_Objects:Number',
    'Boolean' : 'http://developer.mozilla.org/en/docs/Core_JavaScript_1.5_Reference:Global_Objects:Boolean',
    'HTMLElement' : 'http://www.w3.org/TR/DOM-Level-2-HTML/html.html#ID-58190037'
}
    

