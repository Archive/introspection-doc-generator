//<script type="text/javscript">

const Gio     = imports.gi.Gio;
const GLib    = imports.gi.GLib;

const XObject = imports.XObject.XObject;
const console = imports.console.console;

const File    = imports.File.File;

/**
 * Template 
 * 
 * 
 */

var Template = XObject.define(

    function(cfg) {
        XObject.extend(this, cfg);

        //this.templateFile = templateFile;

        if (!this.templateFile || !this.Link) {
            throw  "No templateFile or Link sent to Template Constructor..";
        }
        //console.log("this.templateFile: " + this.templateFile);

        //this.template     = Gio.simple_read(this.templateFile);
        this.template     = File.read(this.templateFile);
        //console.log("this.template: " + this.template);

        this.templateName = GLib.path_get_basename(this.templateFile);
        this.code = "";
        this.parse();
    },

    Object,

    {
        parse : function() {

            console.log("Parsing template: " + this.templateName);
            this.template = this.template.replace(/\{#[\s\S]+?#\}/gi, "");
            this.code     = "var output=``" + this.template;

            this.code = this.code.replace(
                /<for +each="(.+?)" +in="(.+?)" *>/gi, 
                function (match, eachName, inName) {
                    return "``;\rvar $"+eachName+"_keys = keys("+inName+");\rfor(var $"+eachName+"_i = 0; $"+eachName+"_i < $"+eachName+"_keys.length; $"+eachName+"_i++) {\rvar $"+eachName+"_last = ($"+eachName+"_i == $"+eachName+"_keys.length-1);\rvar $"+eachName+"_key = $"+eachName+"_keys[$"+eachName+"_i];\rvar "+eachName+" = "+inName+"[$"+eachName+"_key];\routput+=``";
                }
            );

            this.code = this.code.replace(/<if test="(.+?)">/g, "``;\rif ($1) { \routput+=``");
            this.code = this.code.replace(/<else\s*\/>/g, "``;} \relse\r{ \routput+=``");

            this.code = this.code.replace(/<\/(if|for)>/g, "``;\r};\routput+=``");

            //File.write("/tmp/jstookit_eval_"+this.templateName+".4.js", this.code);

            this.code = this.code.replace(
                /\{\+\s*([\s\S]+?)\s*\+\}/gi,
                function (match, code) {
                    code = code.replace(/"/g, "``"); // prevent qoute-escaping of inline code
                    code = code.replace(/(\r?\n)/g, " ");
                    return "``+ \r("+code+") +\r``";
                }
            );
            //File.write("/tmp/jstookit_eval_"+this.templateName+".6.js", this.code);

            this.code = this.code.replace(
                /\{!\s*([\s\S]+?)\s*!\}/gi,
                function (match, code) {
                    code = code.replace(/"/g, "``"); // prevent qoute-escaping of inline code
                    code = code.replace(/(\n)/g, " "); // remove quotes..
                    return "``; "+code+";\routput+=``";
                }
            );
           //File.write("/tmp/jstookit_eval_"+this.templateName+".7.js", this.code);
            this.code = this.code+"``;";

            this.code = this.code.replace(/(\r?\n)/g, "\\n");
            this.code = this.code.replace(/"/g, "\\\"");

            this.code = this.code.replace(/``/g, "\"");
            this.code = this.code.replace(/\\r/g, "\n");
            //File.write("/tmp/jstookit_eval_"+this.templateName+".9.js", this.code);
            this.code = this.code.replace(/\r/g, "\n\n");
        },

        toCode : function() {
            return this.code;
        },

        keys : function(obj) {
            var keys = [];
            if (obj && obj.constructor.toString().indexOf("Array") > -1) {
                for (var i = 0; i < obj.length; i++) {
                    keys.push(i);
                }
            }
            else {
                for (var i in obj) {
                    keys.push(i);
                }
            }
            return keys;
        },

        values : function(obj) {
            var values = [];
            if (obj.constructor.toString().indexOf("Array") > -1) {
                for (var i = 0; i < obj.length; i++) {
                    values.push(obj[i]);
                }
            }
            else {
                for (var i in obj) {
                    values.push(obj[i]);
                }
            }
            
            
            return values;
        },

        process : function(data) {
            
            //console.log("processing template");
            var keys          = this.keys;
            var values        = this.values;
            
            var makeSortby    = this.makeSortby;
            var makeSignature = XObject.createDelegate(this.makeSignature, this);
            var summarize     = this.summarize ;
            var makeFuncSkel  = this.makeFuncSkel;
            var resolveLinks  = this.resolveLinks;
            var makeImage     = this.makeImage;

            // usefull for cross refing..
            Template.data = data;

            var Link = this.Link;
            //var Options = imports.Options ? imports.Options.Options : false;

            try {
                eval(this.code);
            } catch (e) {
                 //Gio.simple_write('/tmp/template.js', this.code);
                 File.write('/tmp/template.js', this.code);
                 //Seed.print('in /tmp/template.js');
                 console.log('in /tmp/template.js');
                 throw e;
                 //Seed.quit();
            }

            //File.write("/tmp/jstookit_eval.js", this.code);
            //try {
            //eval('include     "/tmp/jstookit_eval.js";');
            //includeJs("/tmp/jstookit_eval.js");
                //eval(this.code);
            // console.log("done eval of template");

            return output;
        },

        isdefined : function (typ) {
            return typ != 'undefined';
        },

        summarize : function(desc) {
            if (typeof desc != "undefined")
                return desc.match(/([\w\W]+?\.)[^a-z0-9]/i)? RegExp.$1 : desc;
            else
                return '';
        },

        /** make a symbol sorter by some attribute */
        makeSortby : function(attribute) {
            return function(a, b) {
                if (a[attribute] != undefined && b[attribute] != undefined) {
                    a = a[attribute]; //.toLowerCase();
                    b = b[attribute];//.toLowerCase();
                    if (a < b) return -1;
                    if (a > b) return  1;
                    //return 0;
                }
                return 0;
            }
        },

        makeImage : function(alias) {
            ///  http://library.gnome.org/devel/gtk/stable/notebook.png
            var ns  = alias.split('.').shift();
            var cls = alias.split('.').pop().toLowerCase();
            if (ns != 'Gtk' ) {
                return '';//ns;
            }
            return '<img class="class-picture" src="http://library.gnome.org/devel/gtk/stable/' + cls + '.png">';
        },

        makeSignature : function(params) {
            if (!params) return "()";
            var Link = this.Link;
            var signature = "("	+
                params.filter(
                    function($) {
                        return $.name.indexOf(".") == -1; // don't show config params in signature
                    }
                ).map(
                    function($) {
                        $.defaultValue = typeof($.defaultValue) == 'undefined' ? false : $.defaultValue;
                        
                        return "" +
                            ($.isOptional ? "[" : "") +
                            (($.type) ? 
                                (new Link().toSymbol(
                                    (typeof($.type) == 'object' ) ? 'Function' : $.type
                                )) + " " :  ""
                            )   + 
                            "<B><i>" +$.name + "</i></B>" +
                            ($.defaultValue ? "=" +item.defaultValue : "") +
                            ($.isOptional ? "]" : "");
                        
                         
                    }
                ).join(", ")
            +
            ")";
            return signature;
        },

        makeFuncSkel :  function(params) {
            if (!params) return "function ()\n{\n\n}";
            return "function ("	+
                params.filter(
                    function($) {
                        return $.name.indexOf(".") == -1; // don't show config params in signature
                    }
                ).map( function($) { return $.name == 'this' ? '_self' : $.name; } ).join(", ")
            +
            ")\n{\n\n}";

        },

        /** Find symbol {@link ...} strings in text and turn into html links */
        resolveLinks : function (str, from) {

            if (!str || typeof(str) == 'undefined') {
                return '';
            }

            // gtk specific. now..
            // @ -> bold.. - they are arguments..

            str = str.replace(/@([a-z_]+)/gi,
                function(match, symbolName) {
                    return '<b>' + symbolName + '</b>';
                }
            );

            // constants.
            str = str.replace(/%([a-z_]+)/gi,
                function(match, symbolName) {
                    return '<b>' + symbolName + '</b>';
                }
            );

            str = str.replace(/#([a-z_]+)/gi,
                function(match, symbolName) {
                    return '<b>' + symbolName + '</b>';
                    // this should do a lookup!!!!
                    /// it could use data in the signature to find out..
                    //return new Link().toSymbol(Template.data.ns + '.' + symbolName);
                }
            );

            str = str.replace(/\n/gi, '<br/>');

            /*
            str = str.replace(/\{@link ([^} ]+) ?\}/gi,
                function(match, symbolName) {
                    return new Link().toSymbol(symbolName);
                }
            );
            */
            /*
            str = str.replace(/\{([a-z\.\/]+)\}/gi,
                function(match, symbolName) {
                    //println("got match " + symbolName);
                    bits = symbolName.split('/');
                    var mret = '';
                    for(var i = 0; i < bits.length; i++) {
                        
                        mret += (mret.length ? '&nbsp;|&nbsp;' : '') + new Link().toSymbol(bits[i]);
                    }
                    
                    return mret; //new Link().toSymbol(symbolName);
                }
            );
            */
            // look for aaaa.\S+  ??? this might work???
            /*
            str = str.replace(/\([a-z]+\.\S+)/gi,
                function(match, symbolName) {
                    return new Link().toSymbol(symbolName);
                }
            );
            */

            return str;
        }
});
