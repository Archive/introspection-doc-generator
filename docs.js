//<Script type="text/javascript">

const Gtk  = imports.gi.Gtk;
const Gio  = imports.gi.Gio;
const Gdk  = imports.gi.Gdk;
const GLib = imports.gi.GLib;


// generic libraries
const XObject     = imports.XObject.XObject;
const File        = imports.File.File; 
const console     = imports.console.console; 
const Template    = imports.JsTemplate.Template.Template; 

// Introspecion specific..
const NameSpace   = imports.Introspect.NameSpace.NameSpace; 
const Link        = imports.Introspect.Link.Link; 


var isSeed = typeof(Seed) != 'undefined';
function argv(n) {
    if (isSeed)
        return Seed.argv[n];
    else
        return ARGV[n-2]; // running gjs from gnome-shell 3 (~/binjhbuild run ..)
                          // make args shift 2 place
}

//var outputdir = Seed.argv[2];
var outputdir = argv(2);
if (!outputdir) {
    throw {
        name   : "ArgumentError",
        message: "No output directory specified on the command line\n"           +
                 "Usage:\n"                                                      +
                 "\t<seed|gjs> docs.js <output_dir> [root_gir_name_list]\n"      +
                 "\toutput_dir          - ex: /var/www/seed \n"                  +
                 "\tgir_root_name_list  - comma separated gir root name ex: Gtk,Gio, ...\n"
    };
}


if (!File.isDirectory(outputdir)) {
    console.log("Creating directory " + outputdir);
    File.mkdir(outputdir);
};


// Which libraries to build.
var ns_list = NameSpace.namespaces();
//if (typeof(Seed.argv[3]) == 'string') {
if (typeof(argv(3)) == 'string') {
//    console.log(Seed.argv.length);
//    ns_list = Seed.argv[3].split(',');
    ns_list = argv(3).split(',');
}
ns_list = ns_list.sort();

// let's try and load them, so we find out early what will fail.
//print("loading library to make sure it works.");
ns_list.forEach(function(ns_name)
{
    // do not try and load Epiphany as it depends on webkit, which is not in ns_list????
    if (ns_name == 'Epiphany') {
        return;
    }
    var core = imports.gi[ns_name];
});

var script_path = GLib.get_current_dir();

// which languages do we want to output for.
var langs = [];
//File.list(__script_path__ + '/templates/').forEach(function(f) {
File.list(script_path + '/templates/').forEach(function(f) {
    // skip file
    //if (!File.isDirectory(__script_path__ + '/templates/' + f)) {
    if (!File.isDirectory(script_path + '/templates/' + f)) {
        return;
    }

    // skip dir
    if (f == 'resources') {
        return;
    }

    //console.log("lang: " + f);
    langs.push({
        name               : f,
        cls_template       : new Template({
                                            //templateFile : __script_path__ + '/templates/' + f + '/class.html',
                                            templateFile : script_path + '/templates/' + f + '/class.html',
                                            Link         : Link // links might be specific to languages..
                                         }),
        cls_ix_template    : new Template({
                                            //templateFile : __script_path__ + '/templates/' + f + '/class_ix.html',
                                            templateFile : script_path + '/templates/' + f + '/class_ix.html',
                                            Link         : Link // links might be specific to languages..
                                         }),
        reference_template : new Template({
                                            //templateFile : __script_path__ + '/templates/' + f + '/references.html',
                                            templateFile : script_path + '/templates/' + f + '/references.html',
                                            Link         : Link // links might be specific to languages..
                                         }),
    });
});


/*
var cls_template       = new Template(__script_path__ + '/templates/class.html');
var cls_ix_template    = new Template(__script_path__ + '/templates/class_ix.html');
var reference_template = new Template(__script_path__ + '/templates/references.html');
*/

print("Looping throught namespaces");
var ns_idx = [];
ns_list.forEach(function(ns_name) 
{
    
    //if (ns_idx.length) {         return ;/* do one - for testing */ } 

    // need to load the lib before introspecting it later
    //var core = imports.gi[ns_name];

    var idx = {name: ns_name};
    console.log("START:" + ns_name);

    var ns = NameSpace.ns(ns_name);

    // gir goes in top level...
    if (File.exists(ns.gir_file)) {
        console.log("gir_file: " + ns.gir_file);
        File.copyFile(ns.gir_file, outputdir + '/'+ ns.gir_filename, Gio.FileCopyFlags.OVERWRITE);
    }

    langs.forEach(function(lang) {

        //console.log("lang.name: " + lang.name);

        //ns['left_bar'] = lang.cls_ix_template.process(ns);
        ns.left_bar = lang.cls_ix_template.process(ns);

        // create lang dir if needed
        if (!File.isDirectory(outputdir + '/' + lang.name)) {
            console.log("Creating directory: " + outputdir + '/' + lang.name);
            File.mkdir(outputdir + '/' + lang.name);
        };

        // namespace template
        //Gio.simple_write(outputdir + '/'+ lang.name + '/' + ns_name + '.html', lang.cls_template.process(ns));
        File.write(outputdir + '/' + lang.name + '/' + ns_name + '.html', lang.cls_template.process(ns));

        // left bar index of elements in namespace...
        //Gio.simple_write(outputdir + '/'+ lang.name + '/_ix_'+ ns_name + '.shtml', lang.cls_ix_template.process(ns));
        File.write(outputdir + '/' + lang.name + '/_ix_'+ ns_name + '.shtml', lang.cls_ix_template.process(ns));
    });

    var actions = {
        'objects'    : 'Class',
        'interfaces' : 'Interface',
        'structs'    : 'Struct',
        'unions'     : 'Union',
        'enums'      : 'Enum'
    };

    for (var i in actions) {
        // we flag GLib as a GObject lib...
        idx[i]= ns_name == 'GLib' ? 1 : ns[i].length ;
        
        ns[i].forEach( function(n) {
            
            //print('NameSpace.factory(' + actions[i] +','+ns_name+','+n);
            var odata = XObject.extend(
                NameSpace.factory(actions[i], ns_name, n),
                { 'left_bar' :ns['left_bar'] }
            );
            
            langs.forEach(function(lang) {
                //Gio.simple_write(outputdir +  '/'+ lang.name + '/' + ns_name + '.' + n + '.html',
                //    lang.cls_template.process(odata)
                //)
                File.write(outputdir +  '/'+ lang.name + '/' + ns_name + '.' + n + '.html',
                    lang.cls_template.process(odata)
                )
            });
            console.log(ns_name + '.' +n);
        }); 
    }
    ns_idx.push(idx);
});

var refs           = '';
var html_file_path = '';
var html           = '';

// output cross reference data..
langs.forEach(function(lang) {

    for (var i in NameSpace.references) {

        html_file_path = [ outputdir, lang.name, i + '.html'].join('/');

        if (i == 'undefined') {
          console.log("Undefined name space - ignored");
          continue;
        }

        if (!File.isFile(html_file_path)) {
          console.log("No HTML file " + html_file_path + " to insert references into - ignored");
          continue;
        }

        refs = lang.reference_template.process(NameSpace.references[i]);

        // HTML to put refs into
        html =  File.read(html_file_path);

        // do the replacement
        html = html.replace(/\<!--references--\>/, refs);

        // write back to file
        //Gio.simple_write(html_file_path, html);
        File.write(html_file_path, html);

    }
});

// set up index and resources.
langs.forEach(function(lang) {
    var ix_template = new Template({
                                    //templateFile : __script_path__ + '/templates/' + lang.name + '/index.html',
                                    templateFile : script_path + '/templates/' + lang.name + '/index.html',
                                    Link : Link, // lang specifc?
                                });

    //Gio.simple_write(outputdir + '/' + lang.name +  '/index.html', ix_template.process(ns_idx));
    File.write(outputdir + '/' + lang.name +  '/index.html', ix_template.process(ns_idx));
    //File.silentRecursiveCopy(__script_path__ + '/templates/resources/',
    File.silentRecursiveCopy(script_path + '/templates/resources/',
                             outputdir + '/'  + lang.name ,
                             Gio.FileCopyFlags.OVERWRITE);
});
