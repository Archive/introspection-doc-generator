/**
 * This is actually part of Introspect/Namespace doc code
 *
 * however since it uses some mozilla specific syntax (doc.namespace.*)
 * it has to be included manually.
 *
 *
 */


function gjs_walk(gir) {
       // clip XML declaration as per instruction at https://developer.mozilla.org/en/E4X
       // to work around bug https://bugzilla.mozilla.org/show_bug.cgi?id=336551
       //gir = gir.replace(/^<\?xml\s+version\s*=\s*(["'])[^\1]+\1[^?]*\?>/, "");    // ";
       // the regex above take too long
       gir = gir.replace(/^<\?xml version="1.0"\?>/, '');
       //console.log('YYY : ');
   
       // clip gtk namespace declaration as it confuse the parser
       gir = gir.replace(/xmlns=\"http:\/\/www.gtk.org\/introspection\/core\/1.0\"/, "");
       //console.log('ZZZ : ');
   
   
       var doc = new XML(gir);
   
       var ret = {};
       var c = doc.namespace.*;
       for (var i in c) {
           if (c[i].function) {
               ret[c[i].@name] = c[i].doc.toString();
   
               if (c[i]['return-value']) {
                   var idxstr  = c[i].@name + '.return-value';
                   ret[idxstr] = c[i]['return-value'].doc.toString();
                   //console.log("=== XML: " +i+ '/' + ret[idxstr] + '/' + c[i] + '/' + idxstr);
               }
   
               if (c[i].parameters) {
                   for (var j in c[i].parameters.parameter) {
                       var idxstr  = c[i].@name + '.' +  c[i].parameters.parameter[j].@name;
                       ret[idxstr] = c[i].parameters.parameter[j].doc.toString();
                       //console.log('XXX : ' + c[i].parameters.parameter[j] + '///' + idxstr);
                   }
               }
           }
   
           // alias not in code but in gir
           //if (c[i].alias) {
           //    var idxstr  = c[i].@name;
           //    ret[idxstr] = c[i].alias.doc.toString();
           //    //console.log('YYY : ' + c[i].alias + '///' + idxstr);
           //}
   
           // doc not in gir
           //if (c[i].enumeration) {
           //    // ,,,
           //}
           //if (c[i].constant) {
           //    // ..,
           //}
       }
   
       return ret;
   }