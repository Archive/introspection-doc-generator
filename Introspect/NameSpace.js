//<script type="text/javascript">
//Gtk = imports.gi.Gtk;

const GI      = imports.gi.GIRepository;
const GLib    = imports.gi.GLib;

 
const File    = imports.File.File;
const XObject = imports.XObject.XObject;
const console = imports.console.console;
//const Constant = imports.Constant;

// BC/FC
if (!GI.Repository) {
    GI.Repository        = GI.IRepository;
    GI.FunctionInfoFlags = GI.IFunctionInfoFlags;
    GI.InfoType          = GI.IInfoType;
    GI.TypeTag           = GI.ITypeTag;

    GI.IBaseInfo.prototype.get_name = function(n) {
        return GI.base_info_get_name(this, n);
    }

    GI.IBaseInfo.prototype.get_namespace = function(n) {
        return GI.base_info_get_namespace(this, n);
    }

    GI.IBaseInfo.prototype.get_attribute = function( n) {
        return GI.base_info_get_attribute(this, n);
    }
}

//NameSpace = {
var NameSpace = {

    references : {},

    namespaces : function(ns) {
        // this should be a class of it's own...
        this.references[ns] = []; // technically not needed - but fills in files..
        // get this from GI... (it's the path..)
        var ret = [];

        function scanGir(dir) 
        {
            if (!GLib.file_test(dir, GLib.FileTest.EXISTS)) {
                return;
            }

            File.list(dir).forEach(function(fn)
            {
                if (!fn.match(/\.typelib$/)) {
                    return;
                }

                var par = fn.split('-').shift();
                 //console.log('trying ' +  par);

                if (ret.indexOf(par) > -1) {
                     return;
                }
                ret.push(par);
            }); 
        }

        var gi  = GI.Repository.get_default();
        var pth = GI.Repository.get_search_path();

        scanGir(pth[0]);
        ret.sort();
        console.dump(ret);

        return ret;
    },

    ns:  function(ns) {
        var gi  = GI.Repository.get_default();
        var ret = {
            titleType : 'Namespace',
            ns        : ns,
            name      : ns,
            alias     : ns,
            objects   : [],
            functions : [],
            enums     : [],
            structs   : [],
            constants : [],
            unions    : [],

            // so ns looks like class..

            extendsClasses : [], // what it extends...
            childClasses   : [], // what uses it..
            properties     : [],
            constructors   : [],
            methods        : [],
            values         : [], /// really constants.
            signals        : [],
            interfaces     : []
        };

        //console.log("NS: " + ns);
        var n_info = gi.get_n_infos(ns);
        //console.log("n_info: " + n_info);

        for (var i=0; i<n_info; i++) {

            var info = gi.get_info (ns, i);
            //console.log("NAME: " + info.get_name());
            //continue;

            var info_type = GI.base_info_get_type (info);
            // print("Type: " + info_type);

            switch(info_type) {
                case  GI.InfoType.OBJECT:
                    ret.objects.push(info.get_name());
                    this.clsGatherInterfaces(ns , info.get_name());
                    continue;

                case  GI.InfoType.INTERFACE:
                    ret.interfaces.push(info.get_name());
                    continue;

                case  GI.InfoType.FUNCTION:
                    new imports.Introspect.Method.Method(info, ret, 'functions', []);    
                    continue;

                case  GI.InfoType.CALLBACK:
                    new imports.Introspect.Callback.Callback(info, ret, 'callbacks', []);
                    continue;

                case  GI.InfoType.ENUM:
                case  GI.InfoType.FLAGS:
                    ret.enums.push(info.get_name());
                    continue;

                case  GI.InfoType.STRUCT:
                    if (GI.struct_info_is_gtype_struct(info)) {
                        continue;
                    }
                    ret.structs.push(info.get_name());
                    continue;

                case  GI.InfoType.UNION:
                    ret.unions.push(info.get_name());
                    continue;

                case  GI.InfoType.CONSTANT:
                    new imports.Introspect.Constant.Constant(info, ret, 'values', []);
                    continue;


                default:
                    continue;
            }
        }
        //print ("SCAN NAMESPACES ALL DONE");

        //var gi       = GI.Repository.get_default();
        var ver      = gi.get_version(ns);
        var pth      = GI.Repository.get_search_path ();

        // SD replace lib with lib.?.? (match lib64 or lib)
        //var gir_path = pth[0].replace(/lib\/girepository-1.0/, 'share\/gir-1.0');
        var gir_path = pth[0].replace(/lib.?.?\/girepository-1.0/, 'share\/gir-1.0');
        //console.log("gir_path: " + gir_path);

        ret.gir_file     = gir_path + '/'+ ns + '-' + ver + '.gir';
        ret.gir_filename = ns + '-' + ver + '.gir';

        //console.dump(this.ifaceList);

        return ret;
    },

    // store all the interfaces, so we can show a list of them later...
    // called when you list the namespace
    clsGatherInterfaces : function(ns, cls)
    {
       // print("clsGatherInterfaces: " + ns + ", " + cls);
        var gi = GI.Repository.get_default();
        var bb = gi.find_by_name(ns, cls);
        var fullname = ns+'.'+cls;
        this.ifaceList = this.ifaceList || { };

        for(var i =0; i < GI.object_info_get_n_interfaces(bb); i++) {

            var prop = GI.object_info_get_interface(bb,i);

            var add =  prop.get_namespace() + '.' + prop.get_name();
            this.ifaceList[add] = this.ifaceList[add] || [];
            if (this.ifaceList[add].indexOf(fullname) < 0) {
                this.ifaceList[add].push(fullname);
            }
        }
    },

    doc : function(what)
    {
        // invalid or empty namespace..
        var ar = what.split('.');
        var ns = ar.shift();
        what = ar.join('.');
        //print("doc get: " + ns + " what=" + what);
         
        this.commentLoad(ns);
         
        return typeof(this.comments[ns][what]) == 'undefined' ?  '' : this.comments[ns][what];
    },

    comments : {},

    commentLoad : function(ns)
    {

        if (typeof(this.comments[ns]) != 'undefined') {
            return;
        }

        console.log("LOAD DOCS: " + ns);
        var gi = GI.Repository.get_default();
        var ver = gi.get_version(ns);
        if (!ver) {
            this.comments[ns] = {};
            return;
        }
        var ret = {};

        // no idea why this is broken on my build system.
        var  getAttribute = function(n, name){
            var properties = n.properties;
            while (properties){
                if (properties.name == name)
                    return properties.children.content;
                properties = properties.next
            }
            return null;
        }

        function seed_walk (element, path) {

            if (!element) {
                return;
            }

            var n =  getAttribute(element, 'name') ;
            //console.log("WALK: " + n);
            
            if (element.name == 'signal') {
                path += '.signal';
            }

            if (n && ((path != '') || (n != ns))) {
                path += path.length ? '.' : '';
                path += n;
            }

            if (element.name == 'return-value') {
                path += '.return-value';
            }
            if (element.name == 'doc') {
                ret[path] = element.content;
                return;
                // no child nodes of this..
            }
             
            var child = element.children;

            while (child){
                //console.log(child.tag);
                if (child.type == "element"){
                    seed_walk (child, path);
                }
                child = child.next;
            }
        }
        
   
        
        
        
        var gi = GI.Repository.get_default();
        var pth = GI.Repository.get_search_path ();

        var gir_path = pth[0].replace(/lib.?.?\/girepository-1.0/, 'share\/gir-1.0');

        //console.log(fn);
        var  fn = gir_path + '/'+ ns + '-' + ver + '.gir';
        // console.log(fn);

        if (!GLib.file_test(fn, GLib.FileTest.EXISTS)) {
            console.log('missing gir file for docs ' + fn);
            this.comments[ns] = {};

            return;
        }
        if (typeof(Seed) == 'undefined') {
            const gjs_walk = imports.templates.gjs.walk.gjs_walk;
            var gir           = File.read(fn);
            this.comments[ns] = gjs_walk(gir);
         } else {
            const seedxml     = imports.libxml;
            var doc           = seedxml.parseFile(fn);
            seed_walk(doc.root, '');
            this.comments[ns] = ret;
         }
        //console.log('loaded gir file for docs ' + fn);
        //console.log('--------------- DUMP --------------------');
        //console.dump(ret);
        //console.log('--------------- DUMP --------------------');

 
    },

    registry : {},

    factory : function(type, ns, name) {
        if (typeof (this.registry[ns +'.' + name]) == 'undefined') {
            //this.registry[ns +'.' + name] = new imports[type][type](ns,name);
            this.registry[ns +'.' + name] = new imports.Introspect[type][type](ns,name);
            this.registry[ns +'.' + name].load();
        }

        return this.registry[ns +'.' + name];
    }
};
//})();
