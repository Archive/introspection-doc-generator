//<script type="text/javascript"> 
/**
* @class xml.XMLNode
* Xml node 
* @noconstructor
* 
* @cfg {String} name Name of Node ???
* @cfg {XMLNode} children First child (use next property to iterate children
* @cfg {XMLNode} parent Parent node of this one
* @cfg {XMLNode} next Next sibling
* @cfg {XMLNode} prev Previous sibling
* @cfg {String} Text contents of Node
* @cfg {XMLNode} last Last Sibling? or last child - FIXME
* @cfg {String} type - Type of node : element, attribute, text
* @cfg {XMLAttribute} properties First Attribute of node
*/
XMLNode = function() {}

Roo.apply(XMLNode.prototype, {
    
});


/**
* @class xml.XMLDocument
* Xml document 
* @noconstructor
* 
* @cfg {String} name Name of Node ???
* @cfg {XMLNode} children First child (use next property to iterate children
* @cfg {XMLNode} parent Parent node of this one
* @cfg {XMLNode} next Next sibling
* @cfg {XMLNode} prev Previous sibling
* @cfg {String} Text contents of Node
* @cfg {XMLNode} last Last Sibling? or last child - FIXME
* @cfg {String} type - Type of node : element, attribute, text
* @cfg {XMLAttribute} properties First Attribute of node
* @cfg {XMLNode} root Root node of document.
*/


XMLDocument  = function () { };

Roo.extend(XMLDocument, XMLNode, {
    root : { }, 
});


/**
* Create a new xpath context ?
* @param {Object} .....
* @return {XMLNode} returns obj
* @member XMLDocument xpathNewContext
*/

XMLDocument.xpathNewContext = function() { }




   
XMLAttribute  = function () { };
   
Roo.extend(XMLAttribute, XMLNode, {   

});

 
XMLXPathContext  = function() { };
Roo.apply(XMLXPathContext.prototype, {
    value : '',
});

XMLXPathContext.xpathEval = function () {} 
XMLXPathContext.xpathRegisterNs = function () {} 


XMLXPathObj = function() { };
Roo.apply(XMLXPathObj.prototype, {
    value : '',
});



parseFile = function () { }
parseString = function () { }
_nodeProto = { }
   
  
 