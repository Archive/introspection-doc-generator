//<script type="text/javascript">
//Gtk = imports.gi.Gtk;
const GI      = imports.gi.GIRepository;
const GLib    = imports.gi.GLib;
 
const XObject = imports.XObject.XObject;
const console = imports.console.console;

const NameSpace = imports.Introspect.NameSpace.NameSpace;
const Base      = imports.Introspect.Base.Base;




/**
 * Class
 */




var Class = XObject.define(
    function(ns, name) {
        Base.call(this, ns, name);
        //print("Class ctr - parent called");
        this.loadExtends();
        this.loadImplements();
        //console.log("CREATED(Class) " + this.alias);
    },
    Base, 
    {
        titleType : 'Class',
        
        loadExtends : function()
        {
            var bi = this.getBI();
            
            var pi = GI.object_info_get_parent(bi);
            this.extendsClasses = [];
            if (!pi || (pi.get_namespace() == this.ns && pi.get_name() == this.name )) {
                return;
            } 
            this.parent = NameSpace.factory(
                'Class',
                pi.get_namespace(),
                pi.get_name()
            );
            
            this.extendsClasses = [ this.parent ];
            
            
            this.parent.extendsClasses.map(function(p) {
                this.extendsClasses.push(p);
            },this);
            
            if (this.parent) {
                this.parent.addChildClass(this.alias);
            }
            
            
        },
        
        addChildClass : function (n) {
            this.childClasses.push(n);
            if (this.parent) {
                this.parent.addChildClass(n);
            }
        },
        
        
        loadImplements : function()
        {
            var bb = this.getBI();
            this.implementInterfaces = [];
            
            var iflist = [];
            for(var i =0; i < GI.object_info_get_n_interfaces(bb); i++) {
               
                var prop = GI.object_info_get_interface(bb,i);
                 
                
                var iface = NameSpace.factory(
                    'Interface', 
                    prop.get_namespace() , prop.get_name()
                );
                
                
                if (iflist.indexOf(iface.alias) < 0) {
                    //console.log("ADDING IFACE " + iface.alias);
                    iflist.push(iface.alias);
                    this.implementInterfaces.push(iface);
                }
            }
            // ask the first of the parents.. - which will have it loaded already
            if (this.extendsClasses.length) {
                
                //this.extendsClasses[0].loadImplements();
                
                this.extendsClasses[0].implementInterfaces.map( function(si) {
                    
                    if (iflist.indexOf(si.alias) < 0) {
                        //console.log("From extends ("+ si.alias + ") IFACE " + si.alias);
                        iflist.push(si.alias);
                        this.implementInterfaces.push(si);
                    }
                },this);
                
            }
            //console.dump(iflist);
            if (this.alias == 'Atk.NoOpObject') {
            //    throw "exut";
               }
           // console.dump(this.implementInterfaces);
        },
        
        
        
        
        load : function()
        {
            if (this._loaded) {
                return; // already loaded..
            }
            
            if (this.parent) { // load up parents first
                this.parent.load();
            }
            this.implementInterfaces.map(function(iface) {
                iface.load();
            });
            
            
            //console.log("load(Class) " + this.alias);
            // my props..
            var props = [];
            this.genericBuildList('object', 'property', props, 'properties');
            this.genericBuildList('object', 'field', props, 'properties');

            this.genericExtends(  props, 'properties');    
            this.genericImplements( props, 'properties');    
           
            var signals = [];
            this.genericBuildList('object', 'signal', signals, 'signals');
            this.genericExtends(  signals, 'signals');    
            this.genericImplements( signals, 'signals');    
            
            
            NameSpace.references[this.alias] = NameSpace.references[this.alias] || [];
            if (this.alias == 'GObject.Object') {
                this._loaded = true;
                return;
            }
            
            
            this.constructors.push({
                name : '', // not really...
                params:   this.properties.length ?  [ { type :  'Object', be_null :  true,  name : 'properties' } ] : [],
                returns :  [],
                isConstructor : true,
                isStatic : false,
                memberOf : this.alias,
                exceptions : [],
                // in theory..
                desc : ''
            });
            
            
            var methods = [];
            this.genericBuildList('object', 'method', methods, 'methods');
            // dont inherit functiosn or consturctors...
            
            this.genericExtends(  methods, 'methods');    
            this.genericImplements( methods, 'methods');    
            
            
            
            //console.log("loaded(Class) " + this.alias);
              
            this._loaded  = true;
        }
         
         
        
        
        
});
