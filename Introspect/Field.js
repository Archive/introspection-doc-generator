//<script type="text/javascript">
//Gtk = imports.gi.Gtk;
const GI      = imports.gi.GIRepository;
const GLib    = imports.gi.GLib;
 

const XObject     = imports.XObject.XObject;
const console     = imports.console.console;

const NameSpace   = imports.Introspect.NameSpace.NameSpace;
const Basic       = imports.Introspect.Basic.Basic;

 
 

/**
 * Field
 */

var Field = XObject.define(
    function(prop, memberOf, saveto, keylist) {
          
       this.name  =  prop.get_name() ;
        this.type  = this.typeToName(GI.field_info_get_type(prop));
        this.flags =  GI.field_info_get_flags(prop);
        this.memberOf = memberOf.alias;
        memberOf[saveto].push(this);
        keylist.push(this.name);

    },
    Basic,
    {}
);
