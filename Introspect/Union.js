//<script type="text/javascript">

const GI      = imports.gi.GIRepository;



const XObject     = imports.XObject.XObject;
const console     = imports.console.console;
 
const Base        = imports.Introspect.Base.Base;

  
   

/**
 * Union
 */
 
var Union = XObject.define(
    function(ns, name) {
        Base.call(this, ns, name);
       
    }, 
    Base, 
    {
        titleType: 'Union',
        _loaded : false,
        load : function()
        {
            if (this._loaded) {
                return; // already loaded..
            }
            // my props..
            var props = [];
            this.genericBuildList('union', 'field', props, 'properties');
            
            if (GI.union_info_get_size (this.getBI()) > 0 ) { 
                
                this.constructors.push({
                    name : '',
                    params: [],
                    returns :  [],
                    isConstructor : true,
                    isStatic : false,
                    memberOf : this.alias,
                    exceptions : [],
                    desc : ''
                });
            }
            var methods = [];
            this.genericBuildList('union', 'method', methods, 'methods');
              
            this._loaded  = true;
        }
         

});
 