//<script type="text/javascript">
//Gtk = imports.gi.Gtk;
const GI      = imports.gi.GIRepository;

const XObject     = imports.XObject.XObject;
const console     = imports.console.console;

const NameSpace   = imports.Introspect.NameSpace.NameSpace;
const Basic       = imports.Introspect.Basic.Basic;


/**
 * Property
 */

var Property = XObject.define(
    function(prop, memberOf, saveto, keylist) {
        this.propertyType = 'Property';
        var n_original = prop.get_name();
        this.name  =  n_original.replace(/\-/g, '_') ,
        this.type  = this.typeToName(GI.property_info_get_type(prop)),
        this.flags =  GI.property_info_get_flags(prop),
        this.memberOf = memberOf.alias
        memberOf[saveto].push(this);
        keylist.push(this.name);
        this.desc = NameSpace.doc(this.memberOf +'.'+  n_original);
        

        if (typeof(this.type) == 'string' && this.type.indexOf('.') > -1) {
        
            NameSpace.references[this.type] = NameSpace.references[this.type] || [];
            NameSpace.references[this.type].push(this);
        }
        
    },
    Basic
);
