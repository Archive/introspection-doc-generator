//<script type="text/javascript">
//Gtk = imports.gi.Gtk;
const GI      = imports.gi.GIRepository;

const XObject     = imports.XObject.XObject;
const console     = imports.console.console;

const NameSpace   = imports.Introspect.NameSpace.NameSpace;
const Basic       = imports.Introspect.Basic.Basic;

  
 

var Signal = XObject.define(
    function(sig, memberOf, saveto, keylist) {

        this.propertyType  = 'Signal';
        
        var params = this.argsToArrays(sig);
        // add a reference to self...
        params.unshift({
            name : 'self',
            type : memberOf.alias,
            direction : 'in',
            be_null :  false
                
        });
        var n_original  = sig.get_name();
        
        XObject.extend(this,{
            name : n_original.replace(/-/g,'_'),
            params : params,
            memberOf : memberOf.alias,
            exceptions : [],
            returns :   [ { type :  this.typeToName(GI.callable_info_get_return_type(sig)) } ],
            desc : NameSpace.doc(memberOf.alias  +  '.signal.' + n_original)
        });
        memberOf[saveto].push(this);
        keylist.push(this.name);
        
        var addedto = [ memberOf.alias ]; // do not add to self...
       
        for(var i =1; i < params.length;i++) {
            var ty = params[i].type;
            if (typeof(ty) != 'string' || ty.indexOf('.') < 0) {
                continue;
            }
            if (addedto.indexOf(ty) > -1) {
                continue;
            }
            
            
            
            NameSpace.references[ty] = NameSpace.references[ty] || [];
            NameSpace.references[ty].push(this);
            addedto.push(ty);
        }
        
        
        
        
        
    },
    Basic
);



