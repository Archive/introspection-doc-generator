//<script type="text/javascript">
//Gtk = imports.gi.Gtk;
 
const XObject     = imports.XObject.XObject;
const console     = imports.console.console;

const NameSpace   = imports.Introspect.NameSpace.NameSpace;
const Base        = imports.Introspect.Base.Base;

 


/**
 * Interface
 */

var Interface = XObject.define(
    function(ns, name) {
        Base.call(this, ns, name);
       
    },

    Base, 
    {
         titleType: 'Interface',
        _loaded : false,
        load : function()
        {
            if (this._loaded) {
                return; // already loaded..
            }
            // my props..
            var props = [];
            this.genericBuildList('interface', 'property', props, 'properties');
           
           
            var signals = [];
            this.genericBuildList('interface', 'signal', signals, 'signals');
          
            
            var methods = [];
            this.genericBuildList('interface', 'method', methods, 'methods');
            
            
            NameSpace.ifaceList[this.alias] = NameSpace.ifaceList[this.alias] || [];
            this.implementedBy = NameSpace.ifaceList[this.alias];
            
              
           
            this._loaded  = true;
        },
         

});
