//<script type="text/javascript">

const GI      = imports.gi.GIRepository;

const XObject     = imports.XObject.XObject;
const console     = imports.console.console;

const NameSpace   = imports.Introspect.NameSpace.NameSpace;
const Base        = imports.Introspect.Base.Base;

 
 
/**
 * Struct
 */

var Struct = XObject.define(
    function(ns, name) {
        Base.call(this, ns, name);
       
    },

    Base, 
    {
        titleType: 'Struct',
        
        _loaded : false,
        load : function()
        {
            if (this._loaded) {
                return; // already loaded..
            }
            // my props..
            var props = [];
            this.genericBuildList('struct', 'field', props, 'properties');
            
            var methods = [];
            
            
            if (GI.struct_info_get_size (this.getBI()) > 0 ) {
               
            
                
                this.constructors.push({
                    name : '',
                    params: [],
                    returns :  [],
                    isConstructor : true,
                    isStatic : false,
                    memberOf : this.alias,
                    exceptions : [],
                    desc : ''
                });
            }
            
            this.genericBuildList('struct', 'method', methods, 'methods');
              
            this._loaded  = true;
        },
         

});