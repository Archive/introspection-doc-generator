//<script type="text/javascript">
const GI      = imports.gi.GIRepository;


const XObject     = imports.XObject.XObject;
const console     = imports.console.console;

const NameSpace   = imports.Introspect.NameSpace.NameSpace;
const Basic       = imports.Introspect.Basic.Basic;

  
/**
 * Methods, functions or consturctors
 */




var Method = XObject.define(
    function(m, memberOf, saveto, keylist) {
        this.propertyType  = 'Method';
        
        var flags = GI.function_info_get_flags (m);
        var n = m.get_name();
        var n_original = n + '';
        // posibly add: sink, 
        if (n.match(/_(ref|unref)$/) || n.match(/^(ref|unref|weakref|weakunref)$/)) {
            return false; // skip these!
        }

        if(n == 'new') {
            n = 'c_new';
        }

        var retval = [ { 
                name : 0, 
                type :  this.typeToName(GI.callable_info_get_return_type(m)),
                desc : NameSpace.doc(memberOf.alias +'.'+  n_original + '.return-value')
        } ];

        var args = this.argsToArrays(m, retval);

        if ((n == 'c_new') && !args.length && memberOf.constructors.length) {

            memberOf.constructors[0].doc = NameSpace.doc(memberOf.alias +'.'+   n_original);

            return false; // skip.
        }

        //console.log(GI.base_info_get_name(m));
        // console.dump(GI.base_info_get_attribute(m, 'doc') );

        // this is a bit messy, as we probably loose the doc's on new..

        XObject.extend(this, {
            name            : n,
            params          : args,
            returns         :  retval,
            isConstructor   : flags & GI.FunctionInfoFlags.IS_CONSTRUCTOR,
            isStatic        : !(flags & GI.FunctionInfoFlags.IS_METHOD),
            memberOf        : memberOf.alias,
            exceptions      : [],
            desc            : NameSpace.doc(memberOf.alias +'.'+  n_original)
        });
        // add descriptions to the arguments..
        this.params.map(function(p) {
            p.desc = NameSpace.doc(memberOf.alias+'.'+  n_original + '.' + p.name);
            //Seed.print(memberOf.alias + '.' + n_original + '.' + p.name + ':' +  p.desc);
        });

        // collect references...
        var addedto = [ memberOf.alias ]; // do not add to self...

        for(var i =0; i < args.length;i++) {
            var ty = args[i].type;
            if (typeof(ty) != 'string' || ty.indexOf('.') < 0) {
                continue;
            }

            if (addedto.indexOf(ty) > -1) {
                continue;
            }
            NameSpace.references[ty] = NameSpace.references[ty] || [];
            NameSpace.references[ty].push(this);
            addedto.push(ty);
        }

        // decide what to add to...
        if (this.isConstructor) {
            if (this.name.match(/^new_/)) {
                this.name = this.name.substring(4);
            }
            memberOf.constructors.push(this);

            //return;
            return true;
        }

        // return values  = only applicable to non-constructors..
        for(var i =0; i < retval.length;i++) {
            var ty = retval[i].type;
            if (typeof(ty) != 'string' || ty.indexOf('.') < 0) {
                continue;
            }
            if (addedto.indexOf(ty) > -1) {
                continue;
            }
            NameSpace.references[ty] = NameSpace.references[ty] || [];
            NameSpace.references[ty].push(this);
            addedto.push(ty);
        }

        if (this.isStatic) {
            memberOf.functions.push(this);
            //return;
            return true;
        }

        memberOf.methods.push(this);
        keylist.push(this.name);

        return true;
    },

    Basic,

    {}
);
