//<script type="text/javascript">
//Gtk = imports.gi.Gtk;
const GI      = imports.gi.GIRepository;
const GLib    = imports.gi.GLib;
//GObject = imports.gi.GObject;

const XObject = imports.XObject.XObject;
const console = imports.console.console;



/**
 * Base class for Properties, functions, methods etc.
 */


var Basic = XObject.define(
    function( ) {
         // never called?
    },

    Object,

    {
        typeToName : function (type_info) {
            var ty = GI.type_tag_to_string( GI.type_info_get_tag(type_info));
            
            if ((ty == 'void') && GI.type_info_is_pointer(type_info)) {
                return 'void*'; // it's a fake string?!?!!? - slightly naughty...
            }

            if (ty == 'array') {
                // array of what!?!?
                var param_type = GI.type_info_get_param_type (type_info, 0);
                var atype = GI.type_info_get_tag(param_type);
                if (atype == GI.TypeTag.UINT8) {
                    return 'utf8';
                }
                
                return ty;
            }

            if (ty != 'interface') {
                return ty;
            }

            var interface_info = GI.type_info_get_interface (type_info);	
            var interface_type = GI.base_info_get_type (interface_info);
            if (interface_type  == GI.InfoType.CALLBACK) {
                // callback.. 
                var Callback = imports.Introspect.Callback.Callback ;
                var ret=  new Callback(interface_info, this, false, false);
                ret.alias = interface_info.get_namespace() + '.' + interface_info.get_name();

                return ret;
            }

            return  interface_info.get_namespace() + '.' + interface_info.get_name();
        },

        directions : [ "in", "out"," inout"],

        argsToArrays : function(m,  returnArray) 
        {
            var outIntoReturn = false;
            if (returnArray && returnArray.length == 1 && returnArray[0].type == 'void') {
                outIntoReturn = true;
            }
            
            var outArg = {
                name : 'out_values',
                ns : '',
                type : 'Object',
                direction : 'out',
                be_null :  true, // in theory..
                desc : "Return Object, use an empty Object, and the properties will get set as follows",
                properties : []
            };
            
            var args = [];
            var firstOut = -1;
            for(var a_i  =0; a_i   < GI.callable_info_get_n_args(m); a_i++) {
                var arg = GI.callable_info_get_arg(m, a_i);
                
                var direction = this.directions[GI.arg_info_get_direction(arg)];
                
                var add = {
                    name : arg.get_name(),
                    ns : arg.get_namespace(),
                    type : this.typeToName(GI.arg_info_get_type(arg)),
                    direction : direction,
                    be_null :  GI.arg_info_may_be_null(arg) || GI.arg_info_is_optional(arg),
                    desc : arg.get_attribute('doc') || ''
                };
                
                
                add.alias = add.ns + '.' + add.name;
                
                if (direction == 'out') {
                    firstOut = firstOut  > -1 ? firstOut : a_i;
                    if (outIntoReturn) {
                        returnArray.push(add);
                    } else {
                        outArg.properties.push(add);
                    }
                }
                
                
                args.push(add);
            }
            
            // drop all the 'out args from the end of args..
            if (returnArray) {
                    
                while (true) {
                    if (!args.length) {
                        break;
                    }
                    var l = args.pop(); // drop!
                    if (l.direction == 'out') {
                        // got an out arg..
                        continue;
                    }
                    args.push(l); // put it back in (it's not a out).
                    break;
                    
                }
            }
            if (outArg.properties.length) {
                // put the out arg in there...
                if (firstOut >= args.length) {
                    args.push(outArg);
                } else {
                    args[firstOut]  = outArg;
                }
            }
            
             
            // remove void return....
            
            if (returnArray && returnArray.length > 1 && returnArray[0].type == 'void') {
                returnArray.shift();
            }
            
            return args;
        }
});
