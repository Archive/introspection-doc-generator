//<script type="text/javascript">
//Gtk = imports.gi.Gtk;
const GI      = imports.gi.GIRepository;
const GLib    = imports.gi.GLib;

//GObject = imports.gi.GObject;

const XObject = imports.XObject.XObject;
const console = imports.console.console;

const NameSpace = imports.Introspect.NameSpace.NameSpace;
const Basic     = imports.Introspect.Basic.Basic;




/**
 * Base  types methods/interfaces/structs/unions etc.
 */



 
var Base = XObject.define(
   function(ns, name) {
        // fake should not happen?
        
        
        if (typeof(name) == 'undefined') {
            var ar = ns.split('.');
            ns = ar[0];
            name = ar[1];
        }
        this.ns = ns;
        this.name = name;
        this.alias = ns + '.' + name;
        //console.log("CREATE(Base) " + this.alias);
        this._loaded = false;
        
        
        // reset all of these..
        this.signals = [];
        this.methods = [];
        this.constructors = [];
        this.functions= [];
       
        this.values = []; // for enum;g;
        this.constants = [];
        
        this.properties = [];
        this.implementInterfaces = []; // obect - which interface it implements.
        this.implementedBy = []; // interface - which object uses it.
        this.extendsClasses = []; // what it extends...
        this.childClasses = []; // what 
         this.desc = NameSpace.doc(ns+'.'+ name );
        
        
        var gi = GI.Repository.get_default();
        var ver = gi.get_version(ns);
        var pth = GI.Repository.get_search_path ();
        var gir_path = pth[0].replace(/lib.?.?\/girepository-1.0/, 'share\/gir-1.0');
       //console.log(fn);
        this.gir_file = gir_path + '/'+ ns + '-' + ver + '.gir';
        this.gir_filename = ns + '-' + ver + '.gir';
        
    }, 
    Basic, 
    {
        titleType: 'Unknown',
        name :  '',
        ns : '',
       
        signals : [],
        methods : [],
        constructors : [],
        functions: [],
       
        values : [], // for enum
        constants : [],
        
        properties : [],
        implementInterfaces : [], // obect - which interface it implements.
        implementedBy : [], // interface - which object uses it.
        extendsClasses : [], // what it extends...
        childClasses : [], // what 
        
        
        
        
        getBI : function()  {
            
            var gi = GI.Repository.get_default();
            return gi.find_by_name(this.ns, this.name);
        },
         // on, getwhat, simple list (name), variable to add to.
        genericBuildList : function( onwhat, type, keylist, saveto)
        {
            
            //console.log(this.alias +" - ADDING :" + type);
            //if (this[saveto].length) {
            //    Seed.print( "EROROR = it's alreayd loaded why?");
                //throw "oops";
            //   }
            var bb = this.getBI();
            var _this = this;
           //console.log("ADD " + type[0].toUpperCase() + type.substring(1));
            var clname = type[0].toUpperCase() + type.substring(1);
            //var cls = imports[clname][clname];
            var cls = imports.Introspect[clname][clname];
            if (!cls) {
                console.log("COULD NOT FIND Introspect: " + type[0].toUpperCase() + type.substring(1));
               }
                
            var plural = type + 's';
            plural = plural == 'propertys' ? 'properties' : plural;
            
            
            for(var i =0; i < GI[onwhat + '_info_get_n_' + plural ](bb); i++) {            
                var add= new cls(GI[onwhat + '_info_get_' + type ](bb,i), this, saveto, keylist);
                //console.log("CLS :" + this.alias + " EL: " + add.name);
            }
             
            
        },
        // loop through and add stuff from extends..
        genericExtends :  function(  keylist,  saveto)   
        {
            // do not overlay gobject props or methods etc.
            
            if (!this.extendsClasses.length) {
                return;
            }
            if (this.extendsClasses[0].alias == 'GObject.Object') {
                return;
            }
                
            
            var _this = this;
            
            this.extendsClasses[0].load();
            this.extendsClasses[0][saveto].map(function(prop) {
                if (keylist.indexOf(prop.name) < 0) {
                    _this[saveto].push(prop);
                    keylist.push(prop.name);
                }
            },this);
            
        },
        // loop through and add stuff from implements..
        genericImplements :  function(  keylist,  saveto)   {
        
            var _this = this;
            this.implementInterfaces.map(function(iface) {
                
                iface.load();
                
                iface[saveto].map( function(prop) {
                    if (keylist.indexOf(prop.name) < 0) {
                        _this[saveto].push(prop);
                        keylist.push(prop.name);
                    }
                },this);
                
            }, this);
            
        },
                
            
        
});


