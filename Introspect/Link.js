    //<script type="text/javascript">

const console = imports.console.console;

const XObject = imports.XObject.XObject


/** Handle the creation of HTML links to documented symbols.
	@constructor
    * 
    *  
    * 
*/

 
var Link = XObject.define(
    function() {
        this.alias = "";
        this.src = "";
        this.file = "";
        this.text = "";
        this.innerName = "";
        this.classLink = false;
        this.targetName = "";
        
        // statics..
        Link.symbolsDir = Link.symbolsDir || '';
        Link.base = Link.base || '';
        Link.ext= Link.ext || '';
        Link.srcDir= Link.srcDir || '';
            
        
    },
    Object,  {
        
        alias : "",
        src : "",
        file : "",
        text : "",
        innerName : "",
        classLink : false,
        targetName : "",
        
        
        
        toString : function() {
            var linkString;
            var thisLink = this;
            
            if (this.none) {
                return 'none';
            }
            if (this.alias) {
                
                if (typeof(this.builtins[this.alias]) != 'undefined') {
                    this.text = this.alias;
                    return thisLink._makeExternLink(this.builtins[this.alias]);
                }
                
                
                return thisLink._makeSymbolLink(this.alias);
                
                /*
                
                linkString = this.alias.replace(/(^|[^a-z$0-9_#.:-])([|a-z$0-9_#.:-]+)($|[^a-z$0-9_#.:-])/i,
                    function(match, prematch, symbolName, postmatch) {
                        var symbolNames = symbolName.split("|");
                        var links = [];
                        for (var i = 0, l = symbolNames.length; i < l; i++) {
                            thisLink.alias = symbolNames[i];
                            links.push(thisLink._makeSymbolLink(symbolNames[i]));
                        }
                        return prematch+links.join("|")+postmatch;
                    }
                );
                */
            }
            else if (this.extern) {
                linkString = thisLink._makeExternLink(this.extern);
            }
            else if (this.src) {
                linkString = thisLink._makeSrcLink(this.src);
            }
            else if (this.file) {
                linkString = thisLink._makeFileLink(this.file);
            }

            return linkString;
        },
        
        builtins : {
            'Object' : 'https://developer.mozilla.org/en/Core_JavaScript_1.5_Reference/Objects/Object',
            //'Object...' : 'http://developer.mozilla.org/en/docs/Core_JavaScript_1.5_Reference:Global_Objects:Object',
            'Array' : 'https://developer.mozilla.org/en/Core_JavaScript_1.5_Reference/Objects/Array',
            'Function' : 'https://developer.mozilla.org/en/Core_JavaScript_1.5_Reference/Objects/Function',
            'String' : 'https://developer.mozilla.org/en/Core_JavaScript_1.5_Reference/Objects/String',
            'Number' : 'https://developer.mozilla.org/en/Core_JavaScript_1.5_Reference/Objects/Number',
            'Boolean' : 'https://developer.mozilla.org/en/Core_JavaScript_1.5_Reference/Objects/Boolean',
            'HTMLElement' : 'http://www.w3.org/TR/DOM-Level-2-HTML/html.html#ID-58190037'
        },
        
        
        toGnome: function(alias) 
        {
            this.file = "#";
            var pr = alias.split('.');
            var aa = alias;
            var site = 'library.gnome.org';
            var extra = '';
            switch(pr[0]) {
                case 'GIRepository':
                   this.extern = 'http://live.gnome.org/GObjectIntrospection';
                   aa = pr[0];
                   break;
                case 'Gio':   
                case 'GLib':   
                case 'GObject':   
                    aa = pr.length > 1 ? 'G' + pr[1] : pr[0];
                    break;
                case 'GdkPixbuf':   
                    aa = pr.length > 1 ? 'Gdk' + pr[1] : pr[0];
                    break;
                case 'GtkSource':   
                    aa = pr.length > 1 ? pr[0] + pr[1] : 'GtkSourceView';
                    break;
                    
                case 'Gst': 
                    aa = pr.length > 1 ? pr[0] + pr[1] : 'GStreamer';
                    break;
                
                case 'Pango':
                    extra = ' GTK-Doc';
                    break;
                case 'GstController': 
                case 'GstApp': 
                case 'GstAudio': 
                case 'GstBase': 
                case 'GstCheck': 
                case 'GstFft': 
                case 'GstInterfaces': 
                case 'GstNet': 
                case 'GstNetbuffer': 
                case 'GstPbutils': 
                case 'GstRiff': 
                case 'GstRtp': 
                case 'GstRtsp': 
                case 'GstSdp': 
                case 'GstTag': 
                case 'GstVideo': 
                
                    aa = pr.length > 1 ? 'Gst' + pr[1] : pr[0];
                    break;
                    
                case 'Epiphany':     
                    aa = pr.length > 1 ?  pr[1] : pr[0];
                    break;
                case 'WebKit':
                    site = 'webkitgtk.org';
                
                default:
                    
                    aa = alias.replace('.', '');
                    
                    break;
             
            }
            if (!this.extern) {
                this.extern = 'http://www.google.com/search?hl=en&amp;' + 
                        'q=site%3A' + site  + '+' + aa + extra +
                        '&amp;btnI=I%27m+Feeling+Lucky&amp;meta=&amp;aq=f&amp;oq=';
            }
            
            this.text =  aa;
            return this;
        },
        
        target : function(targetName) {
            if (typeof(targetName) != 'undefined') this.targetName = targetName;
            return this;
        },
        inner : function(inner) {
            if (typeof(inner) != 'undefined') this.innerName = inner;
            return this;
        },
        withText:  function(text) {
            if (typeof(text) != 'undefined') this.text = text;
            return this;
        },
        toSrc :function(filename) {
            if (typeof(filename) != 'undefined') this.src = filename;
            return this;
        },
        
        toSymbolLong : function(alias)
        {
            this.show_full = true;
            return this.toSymbol(alias)
        },
        toSymbol : function(alias) {
            
            
            switch(alias) {
                case 'boolean' : this.alias = 'Boolean'; return this;
                case 'int' : this.alias = 'Number'; return this;
                case 'uint' : this.alias = 'Number'; return this;
                
                case 'long' : this.alias = 'Number'; return this;
                case 'ulong' : this.alias = 'Number'; return this;
                
                
                case 'uint8' : this.alias = 'Number'; return this;
                case 'int8' : this.alias = 'Number'; return this;
                
                case 'uint16' : this.alias = 'Number'; return this;
                case 'uint16' : this.alias = 'Number'; return this;
                
                case 'int32' : this.alias = 'Number'; return this;
                case 'uint32' : this.alias = 'Number'; return this;
                
                case 'uint64' : this.alias = 'Number'; return this;
                case 'int64' : this.alias = 'Number'; return this;
                
                
                case 'GType' : this.alias = 'Number'; return this;
                case 'size' : this.alias = 'Number'; return this;
                case 'ssize' : this.alias = 'Number'; return this;
                
                case 'float' : this.alias = 'Number'; return this;
                case 'double' : this.alias = 'Number'; return this;
                case 'time_t' : this.alias = 'Number'; return this;
                // fixme - should find out more details..
                case 'array' : this.alias = 'Array'; return this;
                case 'gslist' : this.alias = 'Array'; return this;
                case 'glist' : this.alias = 'Array'; return this; // homefully.
                case 'ghash' : this.alias = 'Object'; return this; // homefully.
                
                
                case 'error' : this.alias = 'Object'; return this; // homefully.
                case 'filename' : this.alias = 'String'; return this; // hopefully..
                case 'utf8' : this.alias = 'String'; return this;
                
                case 'void' : this.none = true; return this;
                
                
            }
            
            
            if (typeof(alias) != 'undefined') {
                this.alias = new String(alias);
            }
            return this;
        },
        toClass : function(alias) {
            this.classLink = true;
            return this.toSymbol(alias);
        },
        toFile : function(file) {
            if (typeof(file) != 'undefined') this.file = file;
            return this;
        },
        
        
      

        /** prefixed for hashes */


        /** Appended to the front of relative link paths. */


        symbolNameToLinkName : function(symbol) {
            var linker = "";
            if (symbol.isStatic) linker = ".";
            else if (symbol.isInner) linker = "-";
            
            return linker+symbol.name;
        },

        


        /** Create a link to a snother symbol. */
        _makeSymbolLink :  function(alias) 
        {
            var target = '';
            var txt = this.show_full ? alias : alias.split('.').pop();
             return "<a href=\"./"+alias+".html\""+target+">"+txt+"</a>";    
            
            /*
            // look for '/' in alias..
            if (/\//.test(alias)) {
                var bits = alias.split('/');
                var ret = "";
                for(var i=0; i < bits.length; i++) {
                    if (i > 0) {
                        ret +="/";
                    }
                    ret += this._makeSymbolLink(bits[i]);
                }
                return ret;
                
            }
            
            
            */
            
            var linkBase = Link.base + Link.symbolsDir;
            //var linkTo = Link.symbolSet.getSymbol(alias);
            linkTo = "FIXME";
            var linkPath;
            var target = (this.targetName)? " target=\""+this.targetName+"\"" : "";
            
            // is it an internal link?
            if (alias.charAt(0) == "#") {
                linkPath = alias;
                fullLinkPath = alias;
            
            // if there is no symbol by that name just return the name unaltered
            } else if (!linkTo) {
                
                if (typeof(this.builtins[alias]) != 'undefined') {
                    return "<a href=\""+ this.builtins[alias]+"\""+target+">"+alias+"</a>";
                 }
                
                return this.text || alias;
            
            
            // it's a symbol in another file
            } else {

                if (!linkTo.isConstructor && !linkTo.isNamespace) { // it's a method or property
                    linkPath = escape(linkTo.memberOf) || "_global_";
                    linkPath += Link.ext + "#" + this.symbolNameToLinkName(linkTo);
                }
                else {
                    linkPath = escape(linkTo.alias);
                    linkPath += Link.ext + (this.classLink? "":"#" +  "constructor");
                }
                //linkPath = linkBase + linkPath;
                fullLinkPath = linkBase + linkPath;
            }
            
            var linkText = this.text || alias;
            
            var link = {linkPath: linkPath, linkText: linkText, fullLinkPath: fullLinkPath};
            
            //if (typeof PluginManager != "undefined") {
            //    JSDOC.PluginManager.run("onSymbolLink", link);
            //}
            
            return "<a href=\""+link.fullLinkPath+"\""+target+">"+link.linkText+"</a>";
        },


        /** Create a link to a source file. */
         _makeSrcLink : function(srcFilePath) {
            var target = (this.targetName)? " target=\""+this.targetName+"\"" : "";
                
            // transform filepath into a filename
            var srcFile = srcFilePath.replace(/\.\.?[\\\/]/g, "").replace(/[:\\\/]/g, "."); // was _
            var lsrcFile = srcFilePath.replace(/\.\.?[\\\/]/g, "").replace(/[:\\\/]/g, ".");
            var outFilePath = Link.base + Link.srcDir + srcFile.replace(/.js$/, '') + Link.ext;
            
            if (!this.text) this.text = FilePath.fileName(srcFilePath);
            return "<a href=\""+outFilePath+"\""+target+">"+this.text+"</a>";
        },

        /** Create a link to a source file. */
         _makeFileLink : function(filePath) {
            var target = (this.targetName)? " target=\""+this.targetName+"\"" : "";
                
            var outFilePath =  Link.base + filePath;

            if (!this.text) this.text = filePath;
            return "<a href=\""+outFilePath+"\""+target+">"+this.text+"</a>";
        },
        
        /** Create a link to a source file. */
         _makeExternLink : function(filePath) {
            var target = (this.targetName)? " target=\""+this.targetName+"\"" : "";
                
            if (!this.text) this.text = filePath;
            return "<a href=\""+filePath+"\""+target+">"+this.text+"</a>";
        }
        
});