//<script type="text/javascript">
//Gtk = imports.gi.Gtk;
const GI      = imports.gi.GIRepository;
const GLib    = imports.gi.GLib;
 
const XObject     = imports.XObject.XObject;
const console     = imports.console.console;

const NameSpace   = imports.Introspect.NameSpace.NameSpace;
const Base        = imports.Introspect.Base.Base;

  

var Enum = XObject.define(
    function(ns, name) {
        Base.call(this, ns, name);
    },
    Base, 
    {
        titleType: 'Enum',
         _loaded : false,
        load : function()
        {
            if (this._loaded) {
                return; // already loaded..
            }
            
            this.desc = NameSpace.doc(this.alias);
            var bi = this.getBI();
                 
            for(var i =0; i < GI.enum_info_get_n_values(bi); i++) {
               
                var prop = GI.enum_info_get_value(bi,i);
                 
              
                this.values.push({
                    name :  prop.get_name().toUpperCase() ,
                    type :   GI.type_tag_to_string(GI.enum_info_get_storage_type(bi)),
                    value:   GI.value_info_get_value(prop) ,
                    memberOf : this.alias
                });
            }
            
            this._loaded = true;
             
             
            
            
        }
});

 