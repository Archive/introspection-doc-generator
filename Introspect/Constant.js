//<script type="text/javascript">
//Gtk = imports.gi.Gtk;
const GI        = imports.gi.GIRepository;
const GLib      = imports.gi.GLib;

const XObject   = imports.XObject.XObject;
const console   = imports.console.console;

const NameSpace = imports.Introspect.NameSpace.NameSpace;
const Basic     = imports.Introspect.Basic.Basic;

 
 

/**
 * Constant
 */


var Constant = XObject.define(
    function(prop, memberOf, saveto, keylist) {

        this.name  = prop.get_name();
        var tif    = GI.constant_info_get_type(prop);
        var ty     = GI.type_tag_to_string( GI.type_info_get_tag(tif));
        this.type  = this.typeToName(GI.constant_info_get_type(prop));

        ///this.flags =  GI.property_info_get_flags(prop),

        this.value= 'UNKNOWN';

        if (ty != 'interface') {
            var argm = new GI.Argument();
            //var argm = new GI._Argument();
            //var argm;
            GI.constant_info_get_value(prop, argm);
            if (ty != 'utf8') {
                this.value = argm.v_long;
            } else {
                this.value = argm.v_string;
            }
        } 
         
        this.desc = NameSpace.doc(memberOf.alias+'.'+   this.name)

        memberOf[saveto].push(this);
        keylist.push(this.name);
    },

    Basic,

    {}
);
