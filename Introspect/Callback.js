//<script type="text/javascript">
//Gtk = imports.gi.Gtk;
const GI      = imports.gi.GIRepository;
const GLib    = imports.gi.GLib;


const XObject = imports.XObject.XObject;
const console = imports.console.console;


const NameSpace = imports.Introspect.NameSpace.NameSpace;
const Basic     = imports.Introspect.Basic.Basic;



var Callback = XObject.define(
    function(sig, memberOf, saveto, keylist) {

        
        var params = this.argsToArrays(sig);
        // add a reference to self...
        /*params.unshift({
            name : 'self',
            type : memberOf.alias,
            direction : 'in',
            be_null :  false
                
        });
        */
        
        XObject.extend(this,{
            name : sig.get_name(),
            params : params,
            //memberOf : memberOf.alias,
            exceptions : [],
            returns :   [ { type :  this.typeToName(GI.callable_info_get_return_type(sig)) } ]            
            
        });
        this.desc =  NameSpace.doc(memberOf.alias +'.'+   this.name);
        //memberOf[saveto].push(this);
        //keylist.push(this.name);
        
    },
    Basic,
    {}
);

