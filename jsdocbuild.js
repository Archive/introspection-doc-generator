/**
 * JSDOC bootstrap.. - documents javascript (not introspection)
 *
 * <script type="text/javascript">
 */

// this files nice and simple 
Options = imports.JSDOC.Options.Options;
BuildDocs = imports.JSDOC.BuildDocs.BuildDocs;


Options.parseArgv(Seed.argv);
BuildDocs.build(Options);



