#!/bin/sh
ls /usr/share/gir-1.0/ | sed s/.gir// | awk \
    '{ print "g-ir-compiler /usr/share/gir-1.0/" $1 ".gir -o /usr/lib/girepository-1.0/" $1 ".typelib" }' \
    | sh
